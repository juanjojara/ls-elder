/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    stopBackgroundGeoLocation: function() {
        bgGeo.stop();
    },

    configureBackgroundGeoLocation: function() {
        // Your app must execute AT LEAST ONE call for the current position via standard Cordova geolocation,
        //  in order to prompt the user for Location permission.
        window.navigator.geolocation.getCurrentPosition(function(position) {
            console.log('Location from Phonegap JJJ');
            console.log('[js] BackgroundGeoLocation config:  ' + position.coords.latitude + ',' + position.coords.longitude);

            if ((localStorage.getItem("tkn") === null)) {
                console.log("No TOKEN");
            }else{
                console.log("Yes TOKEN: " + localStorage.getItem("tkn"));
            }
            if ((localStorage.getItem("location_setting") === null)) {
                console.log("No LOC SET");
            }else{
                console.log("Yes LOC SET: " + localStorage.getItem("location_setting"));
            }
            if ((localStorage.getItem("sharing_setting") === null)) {
                console.log("No SHA SET");
            }else{
                console.log("Yes SHA SET: " + localStorage.getItem("sharing_setting"));
            }

            reverseGeolocation(position.coords.latitude, position.coords.longitude, true, "Initializing... ");
            
        });

        var bgGeo = window.plugins.backgroundGeoLocation;

        /**
        * This would be your own callback for Ajax-requests after POSTing background geolocation to your server.
        */
        var yourAjaxCallback = function(response) {
            ////
            // IMPORTANT:  You must execute the #finish method here to inform the native plugin that you're finished,
            //  and the background-task may be completed.  You must do this regardless if your HTTP request is successful or not.
            // IF YOU DON'T, ios will CRASH YOUR APP for spending too much time in the background.
            //
            //
            bgGeo.finish();
        };

        /**
        * This callback will be executed every time a geolocation is recorded in the background.
        */
        var callbackFn = function(position) {
            var counterTest;
            if (!(localStorage.getItem("counterTest") === null)) { 
                counterTest = parseInt(localStorage.getItem("counterTest"));
            }else{
                counterTest = 1;
            }

            var cnInfo = 'Test nro: ' + counterTest;
            reverseGeolocation(position.latitude, position.longitude, false, cnInfo);

            localStorage.setItem("counterTest", counterTest + 1);

            yourAjaxCallback.call(this);
        };

        var failureFn = function(error) {
            console.log('BackgroundGeoLocation error');
        };
        
        // BackgroundGeoLocation is highly configurable.
        bgGeo.configure(callbackFn, failureFn, {
            url: 'https://ls-gae-api.appspot.com/_ah/api/card/v2/create_card', // <-- only required for Android; ios allows javascript callbacks for your http
            headers: {
                'Authorization': '4848b813435b4ac60829c23543f90a7615d694c0'
            },
            desiredAccuracy: 10,
            stationaryRadius: 20,
            distanceFilter: 200,
            debug: false, // <-- enable this hear sounds for background-geolocation life-cycle.
            notificationTitle: 'Background tracking test', // <-- android only, customize the title of the notification
            notificationText: 'ENABLED!!11' // <-- android only, customize the text of the notification
        });

        // Turn ON the background-geolocation system.  The user will be tracked whenever they suspend the app.
        bgGeo.start();

        // If you wish to turn OFF background-tracking, call the #stop method.
        // bgGeo.stop()

        var share = function(shareInfo, shareLoc, shareLat, shareLon){
            var http = new XMLHttpRequest();
            var url = "https://ls-gae-api.appspot.com/_ah/api/card/v2/create_card";
            var params = { 
                'info': shareInfo,
                'lat': shareLat,
                'lon': shareLon,
                'location': shareLoc + " - " + device.model
            };
            http.open("POST", url, true);

            //Send the proper header information along with the request
            http.setRequestHeader('Content-Type', 'application/json');
            http.setRequestHeader('Authorization','242c3744553868004218448df16f6ea67218f533');

            http.send(JSON.stringify(params));
        };

        var reverseGeolocation = function(lat, lng, confirmation, message){
            var http = new XMLHttpRequest();
            var url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + lng + "&sensor=true";
            http.onreadystatechange = function(){
                if (http.readyState == 4 && http.status == 200){
                    var addr = (JSON.parse(this.responseText)).results[0];
                    var addr_comp = addr.address_components;
                    var revCity;
                    var revRegion;
                    var revCountry;
                    for (i=0; i<addr_comp.length;i++){
                        if ( addr_comp[i].types[0] == "administrative_area_level_1" || addr_comp[i].types[0] == "administrative_area_level_2" ) {
                            revRegion = addr_comp[i].long_name;
                        } else if ( addr_comp[i].types[0] == "country") {
                            revCountry = addr_comp[i].long_name;
                        } else if ( addr_comp[i].types[0] == "administrative_area_level_3" || addr_comp[i].types[0] == "locality") {
                            revCity = addr_comp[i].long_name;
                        }
                    }
                    var curLocation = prepareLocation(revCity, revRegion, revCountry);
                    var curInfo = prepareInfo();
                    if (confirmation == true){
                        window.navigator.notification.confirm(
                            'Share a card now with the following info: ' + curInfo + ' ' + curLocation, 
                            function(confirmButton){
                                if (confirmButton == 1){
                                    share(curLocation, message + ' ' + curInfo, lat, lng);
                                }
                            }, 
                            'Time to Share', 
                            ['Share Now','Maybe Later']);
                    }else{
                        share(curLocation, message + ' ' + curInfo, lat, lng);
                    }
                }
            }
            http.open("GET", url, true);
            //Send the proper header information along with the request
            http.setRequestHeader('Content-Type', 'application/json');
            http.send();
        };

        var prepareLocation = function(curCity, curRegion, curCountry){
            var curLocation = "unavailable";
            var userloc_setting = localStorage.getItem("location_setting");
            if (!(typeof curCity === "undefined") && (location_level(userloc_setting) <= 0)){
                curLocation = curCity;
                if (!(typeof curRegion === "undefined")){
                    curLocation = curLocation + ', ' + curRegion;
                }
                if (!(typeof curCountry === "undefined")){
                    curLocation = curLocation + ', ' + curCountry;
                }
            }else if (!(typeof curRegion === "undefined") && (location_level(userloc_setting) <= 1)){
                curLocation = curRegion;
                if (!(typeof curCountry === "undefined")){
                    curLocation = curLocation + ', ' + curCountry;
                }
            }else if (!(typeof curCountry === "undefined") && (location_level(userloc_setting) <= 2)){
                curLocation = curCountry;
            }
            return curLocation;      
        };

        var prepareInfo = function(){
            var currentdate = new Date(); 
            var datetime = "Last Sync: " + currentdate.getDate() + "/"
                        + (currentdate.getMonth()+1)  + "/" 
                        + currentdate.getFullYear() + " @ "  
                        + currentdate.getHours() + ":"  
                        + currentdate.getMinutes() + ":" 
                        + currentdate.getSeconds();

            var Info = "is at";
            if (currentdate.getHours<=6){
                Info = "is sleeping";
            } else if (currentdate.getHours>= 12 && currentdate.getHours<=14){
                Info = "is having lunch";
            }
            return Info;            
        };

        var location_level = function(loc_level){
            var ret_level;
            switch(loc_level) {
                case "city":
                    ret_level=0;
                    break;
                case "region":
                    ret_level=1;
                    break;
                case "country":
                    ret_level=2;
                    break;
                default:
                    ret_level=3;
            }
            return ret_level;
        };
    }
};
