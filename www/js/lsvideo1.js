$(document).on('deviceready', function() {
    var followersVC = JSON.parse(localStorage.getItem("followers"));
    var selectedFId =  localStorage.getItem("followerIdVC");
    
    //Get the name of the person that is calling
    var followerName = "";
    var followerPic = "";
    for (var i = 0; i < followersVC.length; i++) {
        var curFollower = followersVC[i];
        if (curFollower.id == selectedFId){
            followerName = curFollower.username;
            followerPic = curFollower.picture;
        }
    }

    //Set Picture of the user calling
    $('#picUserCalling').attr("src", followerPic);
    $('#picUserCallingBar').attr("src", followerPic);

    //In case of any error, we go back to slideshow after 60 seconds of no response
    localStorage.setItem("callError", "true");
    setTimeout(backToMain, 1000*60);
        
    //Initialize the globalization plugin and translate the texts
    globalizationApp.globalizationInit(function(){
        //Set text information of the user that is calling
        $('#userCallLabel').html(followerName + " " + globalizationApp.getLanguageValue("callingUserMessage"));
        $('#videoCallLabel').html(globalizationApp.getLanguageValue("videoCallFooterLabel") + followerName);
    });

    try{
        //We GET the session details
        var xmlhttp = new XMLHttpRequest();
        var sessionURL = "https://opentokrtc.com/" + selectedFId + "_" + localStorage.getItem("user_id") + ".json";
        console.log("Follower URL: " + sessionURL);
        xmlhttp.open("GET", sessionURL, false);
        xmlhttp.send();
        var data = JSON.parse( xmlhttp.response );

        //Create the session object
        sessionVC = TB.initSession(data.apiKey, data.sid);
        publisherVC = TB.initPublisher(data.apiKey,'myPublisherDiv');

        //Buttoj to Finish call. Declared here so the publisher gets closed after clicking it
        $('#btnEndVideoCall').on('click', function () {
            finishCall();
        });

        sessionVC.on({
            'streamCreated': function( event ){
                localStorage.removeItem("waitingVideo");        //cancel the timeout exit
                navigator.notification.beep(3);
                if ((localStorage.getItem("autoAnswer") === null)) {
                    localStorage.setItem("ringing", "true");
                    ringingCall();

                    window.navigator.notification.confirm(
                        followerName + " " + globalizationApp.getLanguageValue("callingUserMessage"),
                        function(confirmButton){
                            localStorage.removeItem("ringing");        //cancel the ringing tone
                            if (confirmButton == 1){
                                startCall(event);
                            } else if (confirmButton == 2){
                                if (device.platform != "Android"){
                                    finishCall();
                                }else{
                                    startCall(event);
                                    setTimeout(finishCall,5000);
                                }
                            }
                        }, 
                        globalizationApp.getLanguageValue("callingUserTitle"), 
                        [globalizationApp.getLanguageValue("yes"),globalizationApp.getLanguageValue("no")]
                    );
                }else{
                    startCall();
                }

            },
            'streamDestroyed': function( event ){
                //EndSession(sessionVC);
                console.log("VC SESSION DESTROYED");
                localStorage.setItem("callEndedUnexpectedly", "true");
                setTimeout(callInterrupted, 10000);
            },
            'sessionDisconnected': function( event ){
                //window.location = "main.html";
                console.log("VC SESSION DISCONNECTED");
                localStorage.setItem("callEndedUnexpectedly", "true");
                setTimeout(callInterrupted, 12000);
            },
            'signal': function( event ){
                console.log("Signal data: " + event.data);
                if (event.data == "ciao" + localStorage.getItem("user_id")){
                    EndSession();
                }
            }
        });
        sessionVC.connect(data.token, function(){
            localStorage.removeItem("callError");        //cancel the timeout exit
            //if we do not receive a video stream from the other side we automatically close
            localStorage.setItem("waitingVideo", "true");
            setTimeout(function(){
                if (!(localStorage.getItem("waitingVideo") === null)) {
                    localStorage.removeItem("waitingVideo");
                    finishCall();
                }
            },60000);
        });
    } catch (e) {
        console.log("General ERROR!!!!!!!");
        console.log(e.message);
        window.location = "main.html";
    }
    
});

/*function EndSession(sessionVC){
    sessionVC.disconnect();
}*/

function EndSession(){
    sessionVC.off();
    sessionVC.disconnect();
    localStorage.removeItem("callEndedUnexpectedly");        //cancel the timeout exit
    localStorage.removeItem("waitingVideo");
    window.location = "main.html";
}

function startCall(event){
    //console.log("11111");
    sessionVC.publish( publisherVC );
    $('#myPublisherDiv').hide();
    $('#callerInfo').hide();
    var videoWidth = $(window).width() - 40;
    var videoHeight = $(window).height()*0.8;
    subscriberVC = sessionVC.subscribe( event.stream, 'otherPublisherDiv', {width:videoWidth, height:videoHeight} );
}

function finishCall(){
    sessionVC.signal({
        type: "status",
        data: "ciao" + localStorage.getItem("followerIdVC")
    },
    function(error) {
        console.log("SIGNAL CALLBACK");
        if (error) {
            console.log("signal error: " + error.message);
        } else {
            console.log("Signal Sent");
        }
    });
    publisherVC.destroy();
    EndSession();
}

function callInterrupted(){
    if (!(localStorage.getItem("callEndedUnexpectedly") === null)) {
        EndSession();
    }
}

function backToMain(){
    if (!(localStorage.getItem("callError") === null)) {
        localStorage.removeItem("callError");
        window.location = "main.html";
    }
}

function ringingCall(){
    if (!(localStorage.getItem("ringing") === null)) {
        navigator.notification.beep(3);
        setTimeout(ringingCall, 6000);
    }
}

function init() {
    var apiRoot = "https://ls-gae-api.appspot.com/_ah/api";
    var apisToLoad;
    var callback = function () {
        if (--apisToLoad == 0) {
            var token = localStorage.getItem('tkn');
            gapi.auth.setToken({
                access_token: token
            });
            gapi.client.user.set_status({"status": "busy"}).execute(function(resp) {
                if (!resp.code) {
                    console.log("Status changed to busy");
                }else{
                    console.log("ERROR: Status not changed. " + resp.message);
                }
            });
        }
    }
    document.addEventListener("resume", onResumeApp, false);

    apisToLoad = 1; // must match number of calls to gapi.client.load()
    gapi.client.load('user', 'v2', callback, apiRoot);
}

function onResumeApp(){
    /*console.log("AAAA");
    if (!(publisherVC === null)) {
        console.log("BBBB 1");
        publisherVC.destroy();
        console.log("BBBB 2");
    }
    if (!(subscriberVC === null)) {
        console.log("CCCC 1");
        $('#otherPublisherDiv').hide();
        console.log("CCCC 2");
    }
//    window.plugins.insomnia.keepAwake();
    console.log("DDDD");
    window.location = "main.html";
    */
}