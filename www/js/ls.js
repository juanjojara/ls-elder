var token = ""
var ls = ls || {};
var users = []
var index = 0;
var followers_to_show = 4;
var resize_list = {};
resize_list[1] = {'r': 1, 'c': 1};
resize_list[4] = {'r': 2, 'c': 2};
resize_list[6] = {'r': 2, 'c': 3};
resize_list[12] = {'r': 3, 'c': 4};
resize_list[20] = {'r': 4, 'c': 5};
var len_pics = 1;
var number_of_pic = 1;
var card;
var timer = 40000;
var card_size = 0;
var number_of_card = 0;                 //Indicates the current card number showed for the current follower (pointed by index)
var previous_card_index = 0;            //Indicates how many cards are stored, so users can "swipe back" (limit to 3)
var change_follower = false;            //Indicates that if there are no more cards for the current followers then we need to change follower ( -> index + 1)
var followers_without_cards = true;     //Indicates that at the moment any of the followers has any cards, so we will have to go to slideshow mode
var infoBarTop = 110;                   // 110 is the height of the bottom bar

ls.resize = function () {
//            do some math to adjust the picture.
//          uses row and column parameters
// play with the parameters here.
    //var sH = $(window).height() - 125;  HEIGHT with space for HEADER
    var sH = $(window).height() - infoBarTop;
    if (card_size == 1){
        sH = $(window).height();
        card_size = 0;
    }else{
        card_size = 1;
    }

    $('.fullsize').css('height', sH + 'px');
    //$('#row-followers').css('height', (sH + 75) + 'px');          HIDDEN IN HTML
    $('#photo').css('width', $('.fullsize').width() + 'px');
    var res = resize_list[len_pics];
    if (res == undefined)
        res = resize_list[20];
    r = res['r'];
    c = res['c'];

    //$(".img-card").css('width', ($('#photo').width() / c) - 5);
    $(".img-card").css('width', ($('#photo').width() / c) + 60);
    $('#photo').css('left',-15);
    $('#photo').css('height', sH + 'px');
    //$(".img-card").css('height', ($('#photo').height() / r) - 5);
    $(".img-card").css('height', ($('#photo').height() / r));

    //$('section:not(.header-10-sub):not(.content-11)').css('height', px);xw
}

ls.showFollowers = function () {
    if (!(localStorage.getItem("followers") === null)) {
        //var my_followers = JSON.parse(localStorage.getItem("followers"));
        var result = new EJS({url: 'ejs/followers.ejs'}).render(followers);
        $('#followers').html(result);
    }
}

ls.hideFollowers = function () {
    if (followers.length >= followers_to_show) {
        $("div[id^='user']").hide();
        if ((index + followers_to_show) > followers.length) {
            //this is for the last slice
            for (var i = (followers.length - followers_to_show); i < followers.length; i++)
                $("#user-" + i).show();
        }
        else {

            for (var i = index; i < (index + followers_to_show); i++) {
                $("#user-" + i).show();
            }

        }
    }
}

ls.showCard = function () {
    if (!(localStorage.getItem("tkn") === null)) {
        if (change_follower){
            if (index + 1 == followers.length){
                index = 0;
                if (followers_without_cards){
                    ls.slideShow();
                    return;
                }
            }
            else
                index = (index + 1) % followers.length;
        }
        ls.showUserCardAnimation(index);
        if ((index % followers_to_show) == 0) {
            ls.hideFollowers();
        }
        /*if (change_follower){
            if (index + 1 == followers.length)
                index = 0;
            else
                index = (index + 1) % followers.length;
        }*/
    }
    //if (followers_without_cards >= followers.length){
      //  ls.slideShow();
    //}
}

ls.nextCard = function() {
    console.log("NEXT");
    localStorage.setItem("skipAnimation", parseInt(localStorage.getItem("skipAnimation")) + 1);
    console.log("skipped " + localStorage.getItem("skipAnimation") + " animations");
    ls.showCard();
}

ls.previousCard = function() {
    if (previous_card_index <= 1){
        $("#forbidden").slideDown(1000);
        setTimeout(function(){
            $("#forbidden").slideUp(1000);    
        }, 2000);
        console.log("No cards stored");
        return;
    }
    console.log("PREVIOUS");
    localStorage.setItem("skipAnimation", parseInt(localStorage.getItem("skipAnimation")) + 1);
    console.log("skipped " + localStorage.getItem("skipAnimation") + " animations");
    
    //$("#card").toggle("slide");
    var options = { direction: 'right' };
    $('#card').toggle('slide', options);
    //$("#card").slideToggle();
    setTimeout(function(){
        previous_card_index--;
        previous_card = JSON.parse(localStorage.getItem("stored_card_" + previous_card_index));
        var result = new EJS({url: 'ejs/card.ejs'}).render(previous_card);
        $('#card').html(result);
        //Animate changing postcards
        $("#card").show();
        ls.resize();
        $("#card").hide();
        setTimeout(function(){
            $("#card").toggle("slide");
        }, 200);
        card = previous_card;
        setTimeout(ls.showMap, (timer / 2));
    }, 500);    
}

 /*ls.showMapAnimation = function () {
    $("#card").toggle("clip", {direction:"horizontal"});
    setTimeout(function(){
        ls.showMap();
    }, 600);
}*/

ls.updateCards = function (init) {
    //uopdates the cards, first time (init=true) loads followers and loads all their cards
    gapi.client.user.peopleifollow().execute(function (resp) {
        if (!resp.code) {
            var f_resp = resp.result;
            followers = f_resp.users;
            if (followers === undefined) {
                ls.slideShow();
                return;
            }
            localStorage.setItem("followers", JSON.stringify(followers));
            ls.showFollowers();
            ajax.PopulateListView("delete");

            //for all the followers update the current card.
            for (var i = 0; i < followers.length; i++) {
                var follower = followers[i];
                console.log("Follower: " + follower.id);
//                  Remove old cards
                localStorage.removeItem("card-0-" + follower.id);
                localStorage.removeItem("card-1-" + follower.id);
                localStorage.removeItem("card-2-" + follower.id);

                gapi.client.card.usercardsuserIdn({'userId': follower.id, 'n': 3}).execute(function (resp) {
//                      here we get the cards, we have to store it.
//                      don't use follower.id here. it's async (damn JS)
                    if (!resp.code) {
//                            stringify beacuse it works only with strings
                        for (var j = 0; j < resp.cards.length; j++) {
                            var cardVarName = "card-" + j + "-" + resp.cards[j].user.id;
                            localStorage.setItem(cardVarName, JSON.stringify(resp.cards[j]));
                        }
                        //localStorage.setItem("card-" + resp.user.id, JSON.stringify(resp));
                    }
                });
            }
//                initialize currentcards
            var usercards = [];
//                for all the followers fake the card as -1
            $.each(followers, function (index, follower) {
//                    build the array
                var card_j = {};

                card_j.user = follower.id;
                card_j.card = -1;
                usercards.push(card_j);
            });
//                check if there's a new one
            gapi.client.card.checkcards({"usercards": usercards }).execute(function (resp) {
                if (!resp.code) {
                    usercards = resp.usercards;
//                        store this info in the currentCards (it's an array of (userId,cardId,new)
                    localStorage.setItem("currentCards", JSON.stringify(usercards));
                }
            });
            ////after 5 sec show the card, pretty sure there's something..
            if (init) {
                setTimeout(ls.showCard, 5000);
            }
        }
    });
    
    //    everyhour check new one.
    setTimeout(function () {
        //reset the variables that control card and follower cycles
        number_of_card = 0;
        change_follower = false;
        followers_without_cards = true;
        //reset the followers slideshow
        ls.updateCards(false);
    }, 1000 * 60 * 60);
}

ls.slideShow = function () {
    //We show a fixed slide show if the user does not have any followers yet
    if (!(localStorage.getItem("tkn") === null)) {  //Stop slide show if user logged out
        var cardIndex = Math.floor((Math.random() * 20) + 1);
        var cardPhoto = "img/slideShow/" + cardIndex + ".jpg";    
        card = JSON.parse('{}');
        card.photo = cardPhoto;
        var result = new EJS({url: 'ejs/slideShow.ejs'}).render(card);

        $('#card').html(result);
        ls.resize();
        setTimeout(ls.slideShow, (timer / 2));
    }
}

ls.showUserCardAnimation = function (id) {
    $("#card").toggle("slide");
    //$("#card").slideToggle();
    setTimeout(function(){
        ls.showUserCard(id);
    }, 500);
}

ls.showUserCard = function (id) {    
    if (followers.length > 0){
        var id_u = followers[id].id;
        //get the card from storage
        var card_s = localStorage.getItem("card-" + number_of_card + "-" + id_u);
        if (card_s != undefined) {
            card = JSON.parse(card_s);
            len_pics = card.photo.length;
            var i = Math.floor(Math.random() * (len_pics - number_of_pic));
            card.photo = card.photo.slice(i, i + number_of_pic);
            len_pics = card.photo.length;

            var infoWidth = $(window).width();
            var infoTop = $(window).height();
            
            //Remove wrong text from server. Correct later the wrong test on the Mobile app
            card.location = card.location.replace(" (undefined)", "");
            //Bar on bottom layout
            card.infoBarWidth = infoWidth + 5;
            card.infoBarTop = infoTop-infoBarTop;
            card.infoCircleTop = infoTop*0.8-38;
            card.divWidth = infoWidth;
            card.divHeight = infoTop;
            card.cardAge = cardUtils.dateDiffInDays(card.created);

            card.forbiddenTop = infoTop*0.25;
            card.forbiddenLeft = infoWidth*0.1;
            card.forbiddenSize = infoTop*0.5;
            
            //cosnole.log("CARD USER PIC: " + card.user.picture);
            card.weatherImg = "img/" + cardUtils.weatherImage(card.weather);
            var result = new EJS({url: 'ejs/card.ejs'}).render(card);
            $('#card').html(result);
            var name_div = "#user-" + id;
            $("#followers .active").removeClass("active");
            $(name_div).addClass("active");
            //Animate changing postcards
            $("#card").show();
            ls.resize();
            $("#card").hide();
            setTimeout(function(){
                $("#card").toggle("slide");
                //$("#card").slideToggle();
            }, 200);

            StoreCard(JSON.stringify(card));        //we store the current card in case the user wants to see it again by sliding back
            number_of_card = number_of_card + 1;
            console.log("Card Number: " + number_of_card);
            change_follower = false;
            followers_without_cards = false;
            setTimeout(ls.showMap, (timer / 2));
        }else{
            console.log("No cards");
            number_of_card = 0;
            change_follower = true;
            //followers_without_cards = followers_without_cards + 1;
            setTimeout(ls.showCard, 2000);
        }        
    }else{
        console.log("No Followers");
    }
}

ls.showMap = function () {
    if (localStorage.getItem("skipAnimation") > 0) {
        //localStorage.removeItem("skipAnimation");
        localStorage.setItem("skipAnimation", parseInt(localStorage.getItem("skipAnimation")) - 1);
        console.log("CANCEL SWIPE " + localStorage.getItem("skipAnimation"));
        return;
    }
    if (!(localStorage.getItem("tkn") === null)) {
        //var options = { direction: 'left' };
        //$('#card').toggle('slide', options, 500);
        card.weatherImg = "img/" + cardUtils.weatherImage(card.weather);
        var postcardLansdcape = true;
        var infoWidth = $(window).width();
        var infoTop = $(window).height();
        //console.log("W: " + infoWidth + " - T: " + infoTop);
        //console.log("WB: " + infoWidth*0.8 + " - TB: " + infoTop*0.7);
        card.infoDataHeight = infoTop*0.2;
        card.infoMapHeight = infoTop*0.8;

        card.infoBarWidth = infoWidth*0.8;
        card.infoBarTop = infoTop*0.8;
        card.infoCircleTop = infoTop*0.8-38;
        
        if (postcardLansdcape){
            card.infoStampTop = infoTop*0.04-5;
            card.infoStampLeft = infoWidth*0.85-5;
            card.infoUserPicTop = infoTop*0.04;
            card.infoUserPicLeft = infoWidth*0.85;
            card.infoDataTop = infoTop*0.05;
            card.infoDataLeft = infoWidth*0.25;
            card.infoWeatherTop = infoTop*0.45;
            card.infoWeatherLeft = infoWidth*0.4;

            card.infoAgeTop = infoTop*0.05;
            card.infoAgeLeft = infoWidth*0.02;
        }else{
            card.infoStampTop = infoTop*0.125-10;
            card.infoStampLeft = infoWidth*0.75-10;
            card.infoUserPicTop = infoTop*0.125;
            card.infoUserPicLeft = infoWidth*0.75;
            card.infoDataTop = infoTop*0.5;
            card.infoDataLeft = infoWidth*0.7;
            card.infoWeatherTop = infoTop*0.25;
            card.infoWeatherLeft = infoWidth*0.25;

            card.infoAgeTop = infoTop*0.85;
            card.infoAgeLeft = infoWidth*0.7;            
        }
        card.cardAge = cardUtils.dateDiffInDays(card.created);

        var shortName = card.user.username.substring(0, card.user.username.indexOf(" "));
        var shortLocation = card.location;
        if (card.location.indexOf(",") > 0){
            if (card.location.indexOf(",") == card.location.lastIndexOf(",")){
                shortLocation = card.location.substring(card.location.indexOf(",") + 1);
            }else{
                shortLocation = card.location.substring(0, card.location.indexOf(",")) + card.location.substring(card.location.lastIndexOf(","));
            }
        }

        card.short_location = shortLocation;
        card.short_name = shortName;
        card.divWidth = infoWidth;
        card.divHeight = infoTop;

        //Info Bar vertical on the right
        //var result = new EJS({url: 'ejs/map.ejs'}).render(card);

        //Info Bar horizontal on the top
        var result = new EJS({url: 'ejs/mapH.ejs'}).render(card);
        $('#card').html(result);
        len_pics = 1;
        //$("#card").show();
        ls.resize();
        //$("#card").hide();
        /*setTimeout(function(){
            $("#card").toggle("clip", {direction:"horizontal"});
        }, 200);*/
        setTimeout(function() {
            if (localStorage.getItem("skipAnimation") > 0) {
                localStorage.setItem("skipAnimation", parseInt(localStorage.getItem("skipAnimation")) - 1);
                console.log("CANCEL SWIPE " + localStorage.getItem("skipAnimation"));
                return;
            }
            ls.showCard()
        }, (timer / 2));
    }
}

function StoreCard(cur_card) {
    console.log("Storing Card");
    if (previous_card_index >= 4){
        localStorage.setItem("stored_card_1", localStorage.getItem("stored_card_2"));
        localStorage.setItem("stored_card_2", localStorage.getItem("stored_card_3"));
        localStorage.setItem("stored_card_3", localStorage.getItem("stored_card_4"));
        localStorage.setItem("stored_card_4", cur_card);
    }else{
        previous_card_index++;
        console.log("Storing Card 111");
        localStorage.setItem("stored_card_" + previous_card_index, cur_card);
        console.log("Storing Card 222");
    }
}
