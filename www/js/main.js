$(document).on('deviceready', function() {
    $('#settings_page').hide();
    setTimeout(connectSessions, 5000);
});

$(document).on("pagecreate","#pagedeletefollowers",function(event){
    $(this).on("click", "#followersDeleteList a", function (e) {
        //prevent dialog transition 
        e.preventDefault();
        var sourceId = $(this).closest("li").attr("id");
        var sourceName = $(this).closest("li").attr("user-name");
        var sourcePic = $(this).closest("li").attr("user-pic");
        //set that in #dialogPage's data for later use
        $("#YesNoDialogPage").data("selectedUser", sourceId);
        $("#YesNoDialogPage").data("selectedName", sourceName);
        $("#YesNoDialogPage").data("selectedPic", sourcePic);
        //redirect to dialog
        $.mobile.changePage(this.href, {
            role: "dialog"
        });
    });
});

$(document).on("pagebeforeshow", "#YesNoDialogPage", function () {
    //get the chapter Id from data
    var selectedUser=  $(this).data("selectedUser");
    var selectedName=  $(this).data("selectedName");
    var selectedPic=  $(this).data("selectedPic");
    //do anything you want with it.
    var dlgMsg = globalizationApp.getLanguageValue("dlgMsgRemoveFollower");
    $(this).find('[data-role="content"] p')
        .html(dlgMsg + selectedName);
    $('#picUser').attr("src", selectedPic);

    $('#btnConfirm').on('click', function() {
        followerManagement.removeFollower(selectedUser);
    });
});

$(document).on("pagebeforehide", "#YesNoDialogPage", function () {
    $('#btnConfirm').off();
});

function connectSessions(){
    console.log("Connect to Sessions");
    if (!(localStorage.getItem("followers") === null)) {
        //var followers = JSON.parse(localStorage.getItem("followers"));
        if (followers.length >= 1){
            var xmlhttp = new XMLHttpRequest();
            var sessionURL = "https://opentokrtc.com/lsvideocall" + localStorage.getItem("user_id") + ".json";
            console.log("Follower URL: " + sessionURL);
            xmlhttp.open("GET", sessionURL, false);
            xmlhttp.send();
            var data = JSON.parse( xmlhttp.response );
            sessionPre = TB.initSession(data.apiKey, data.sid);
            //sessionPre.off();
            sessionPre.on({
                'connectionCreated': function( event ){
                    console.log("Someone connected to the session");
                },
                'sessionDisconnected': function( event ){
                    console.log("SESSION FINISHED");
                    setTimeout(connectSessions, 5000);
                },
                'signal': function( event ){
                    console.log("Signal data: " + event.data);
                    if (event.data.indexOf("_") <= 0){
                        sessionPre.signal({
                            type: "status",
                            data: event.data + "_" + localStorage.getItem("user_id")
                        },
                        function(error) {
                            console.log("SIGNAL CALLBACK");
                            if (error) {
                                console.log("signal error: " + error.message);
                            } else {
                                console.log("Signal Sent");
                            }
                        });
                        localStorage.setItem("followerIdVC", event.data);
                        sessionPre.off();
                        //session.disconnect();
                        window.location = "lsvideo.html";
                    }
                }
            });
            sessionPre.connect(data.token, function(){
                console.log("Waiting for a video call");
                gapi.client.user.set_status({"status": "online"}).execute(function(resp) {
                    if (!resp.code) {
                        console.log("Status changed to online");
                    }else{
                        console.log("ERROR: Status not changed. " + resp.message);
                    }
                });
            });
        }
    }
}

function init() {
    var apiRoot = "https://ls-gae-api.appspot.com/_ah/api";
    var apisToLoad;
    var user_id = localStorage.getItem("user_id");
    var callback = function () {
        if (--apisToLoad == 0) {
            var id = sessionStorage.getItem('id');
            var token = localStorage.getItem('tkn');

            //We set the variable that determines how many card changes we need to ignore because the user decided to manually advance to the next card
            localStorage.setItem("skipAnimation", 0);

            gapi.auth.setToken({
                access_token: token
            });
            globalizationApp.globalizationInit(function(){
                //Moved here because globalization is not loaded when it is called and it gives an error
                //Show message for adding followers
                var greetMessage = globalizationApp.getLanguageValue("popupMsgWelcome");
                /* HIDDEN IN HTML
                $("#row-followers").on("tap", function () {
                    navigator.notification.alert(greetMessage, null, globalizationApp.getLanguageValue("popupTitleFollowers"), globalizationApp.getLanguageValue("popupButtonDone"));
                });
                */
                var user_id = localStorage.getItem("user_id");
                if (localStorage.getItem(user_id) === null) {
                    localStorage.setItem(user_id, 1);
                    navigator.notification.alert(greetMessage, null, globalizationApp.getLanguageValue("popupTitleFollowers"), globalizationApp.getLanguageValue("popupButtonDone"));
                }                
            });

            window.plugins.insomnia.keepAwake();
//            call the update cards at the start
            ls.updateCards(true);

            //SHOW - HIDE MENU BAR
            $("#card").on("tap", function () {
                if ($("#menuHeader").is(':hidden')){
                    $("#menuHeader").slideDown(1000);
                    setTimeout(function(){
                        $("#menuHeader").slideUp(1000);    
                    }, 10000);
                }else{
                    $("#menuHeader").slideUp(1000);
                }
                cardUtils.setTap(localStorage.getItem("user_id"));
            });

            $("#card").on("swipeleft", function () {
                cardUtils.setSlideForward(localStorage.getItem("user_id"));
                ls.nextCard();
            });
            $("#card").on("swiperight", function () {
                cardUtils.setSlideBackward(localStorage.getItem("user_id"));
                ls.previousCard();
            });
            
            $('#btnLogout').on('click', function () {
                window.navigator.notification.confirm(
                    globalizationApp.getLanguageValue("dlgConfirmExit"),
                    function (confirmButton) {
                        if (confirmButton == 1) {
                            gapi.client.user.set_status({"status": "offline"}).execute(function(resp) {
                                if (!resp.code) {
                                    console.log("Status changed to offline");
                                }
                            });

                            localStorage.removeItem("tkn");
                            window.plugins.insomnia.allowSleepAgain();
                            window.location = "index.html";
                        }
                    },
                    globalizationApp.getLanguageValue("dlgTitleExit"),
                    [globalizationApp.getLanguageValue("yes"), globalizationApp.getLanguageValue("no")]
                );
            });

            $('#btnSettings').on('click', function () {
                $('#settings_page').show();
                $('#card_page').hide();
                //$('#tapCounter').html(localStorage.getItem(user_id + "_tap"));
            });
            $('#btnDone').on('click', function () {
                if ($('#autoAnswerTrue').is(':checked')) { 
                    localStorage.setItem("autoAnswer", "true");
                } else { 
                    localStorage.removeItem("autoAnswer");
                }
                
                if (!(localStorage.getItem("deletedUser") === null)) {
                    localStorage.removeItem("deletedUser");
                    window.location="main.html";
                }else{
                    $('#card_page').show();
                    $('#settings_page').hide();
                }
            });
            document.addEventListener("resume", onResumeApp, false);

            //Automatic Answer Switch
            if ((localStorage.getItem("autoAnswer") === null)) {
                $('#autoAnswerTrue').attr("checked",false).checkboxradio("refresh");
                $('#autoAnswerFalse').attr("checked",true).checkboxradio("refresh");
            }else{
                $('#autoAnswerFalse').attr("checked",false).checkboxradio("refresh");
                $('#autoAnswerTrue').attr("checked",true).checkboxradio("refresh");
            }
        }
    }

    apisToLoad = 2; // must match number of calls to gapi.client.load()
    gapi.client.load('card', 'v2', callback, apiRoot);
    gapi.client.load('user', 'v2', callback, apiRoot);
}

function onResumeApp(){
    window.location = "main.html";
}
