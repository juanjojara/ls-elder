var platformFile;

var googleapi = {
    authorize: function(options) {
        var deferred = $.Deferred();

        //Build the OAuth consent page URL
        var authUrl = 'https://accounts.google.com/o/oauth2/auth?' + $.param({
            client_id: options.client_id,
            redirect_uri: options.redirect_uri,
            response_type: 'code',
            scope: options.scope
        });

        //Open the OAuth consent page in the InAppBrowser
        var authWindow = window.open(authUrl, '_blank', 'location=no,toolbar=no');

        //The recommendation is to use the redirect_uri "urn:ietf:wg:oauth:2.0:oob"
        //which sets the authorization code in the browser's title. However, we can't
        //access the title of the InAppBrowser.
        //
        //Instead, we pass a bogus redirect_uri of "http://localhost", which means the
        //authorization code will get set in the url. We can access the url in the
        //loadstart and loadstop events. So if we bind the loadstart event, we can
        //find the authorization code and close the InAppBrowser after the user
        //has granted us access to their data.
        $(authWindow).on('loadstart', function(e) {
            var url = e.originalEvent.url;
            var code = /\?code=(.+)$/.exec(url);
            var error = /\?error=(.+)$/.exec(url);

            if (code || error) {
                //Always close the browser when match is found
                authWindow.close();
            }

            if (code) {
                var code_v1=code[1].split("&");
                //Exchange the authorization code for an access token
                $.post('https://accounts.google.com/o/oauth2/token', {
                    code: code_v1[0],
                    client_id: options.client_id,
                    client_secret: options.client_secret,
                    redirect_uri: options.redirect_uri,
                    grant_type: 'authorization_code'
                }).done(function(data) {
                    deferred.resolve(data);
                }).fail(function(response) {
                    deferred.reject(response.responseJSON);
                });
            } else if (error) {
                //The user denied access to the app
                deferred.reject({
                    error: error[1]
                });
            }
        });

        return deferred.promise();
    }
};

//Hide login elements in case there is no Internet Connection
$('#btnGoogle').hide();
$('#btnFb').hide();
$('#btnLocal').hide();
$('#btnNewUser').hide();
$('#loginControls').hide();
$(document).on('deviceready', function() {
    if (device.platform != "Android"){
        platformFile = "main.html";
    }else{
        platformFile = "mainAndroid.html";
    }
    //Action when the app is resumed normally
    document.addEventListener("resume", onResumeApp, false);

    if(navigator.connection.type == Connection.NONE){
        //Globalization init
        globalizationApp.globalizationInit(function(){
            $("#loginPopup p").html(globalizationApp.getLanguageValue("popupNoInternet"));
            setTimeout(function () {
                $('#loginPopup').popup('open', {positionTo: 'window'});
            }, 100);
            setTimeout(function () {
                $('#loginPopup').popup('close');
                $("#loginPopup p").html(globalizationApp.getLanguageValue("popupConnecting"));
            }, 4000);
        });
    }else{
        //If the app was closed without logging out
        if (!(localStorage.getItem("tkn") === null)) {
            window.location = platformFile;
        }else{
            //Globalization init
            globalizationApp.globalizationInit(function(){
                console.log("Globalization Init");
            });
        }
    }

    var $loginLocal = $('#btnLocal');
    var $createLocal = $('#btnNewUser');
    var $loginGoogle = $('#btnGoogle');
    var $loginFb = $('#btnFb');
    openFB.init({appId: '783907758296055'}); // Defaults to sessionStorage for storing the Facebook token

    $loginLocal.on('click', function() {
        console.log("click Local");
        ls.local_login($('#usrnm').val(), $('#pswd').val());
    });

    $createLocal.on('click', function() {
        console.log("click create Local");
        ls.local_create($('#usrnm').val(), $('#pswd').val());
    });

    $loginGoogle.on('click', function() {
        googleapi.authorize({
            client_id: '1063218928873-ecr9rqr477r4tfisdsmqa61a2dhhu2dr.apps.googleusercontent.com',
            client_secret: '9V-ntBjrlVI2noicQkZgMiFf',
            redirect_uri: 'http://localhost',
            scope: 'openid profile email'
        }).done(function(data) {
            console.log("login GOOGLE SUCCESS");
            ls.social_login("GOOGLE", data.access_token);
        }).fail(function(data) {
            //$("#loginPopup p").html("There was an error during the login process." + data.error);
            $("#loginPopup p").html(globalizationApp.getLanguageValue("popupErrorLogin") + data.error);
            setTimeout(function () {
                $('#loginPopup').popup('close');
                //$("#loginPopup p").html("Connecting...");
                $("#loginPopup p").html(globalizationApp.getLanguageValue("popupConnecting"));
            }, 3000);
        });
    });

    $loginFb.on('click', function() {
        openFB.login(
            function(response) {
                if(response.authResponse) {
                    console.log("login FB SUCCESS");
                    ls.social_login("FB", response.authResponse.token);
                } else {
                    //$("#loginPopup p").html("There was an error during the login process." + response.error_description);
                    $("#loginPopup p").html(globalizationApp.getLanguageValue("popupErrorLogin") + response.error_description);
                    setTimeout(function () {
                        $('#loginPopup').popup('close');
                        //$("#loginPopup p").html("Connecting...");
                        $("#loginPopup p").html(globalizationApp.getLanguageValue("popupConnecting"));
                    }, 3000);
                }
            }, {scope: 'public_profile,email'});
    });
});

var ls = ls || {};
ls.social_login = function(platform, token){
    console.log("login " + platform + " - " + token);
    gapi.client.user.oauth_login({"platform":platform,"token": token}).execute(function(resp) {
        if (!resp.code) {
            sessionStorage.setItem('id', resp.id);
            sessionStorage.setItem('token', resp.token);
            localStorage.setItem("tkn", resp.token);
            localStorage.setItem("user_id", resp.id);
            $('#loginPopup').popup('close');
            window.location=platformFile;
        }else{
            //$("#loginPopup p").html("There was an error during the login process." + resp.message);
            $("#loginPopup p").html(globalizationApp.getLanguageValue("popupErrorLogin") + resp.message);
            setTimeout(function () {
                $('#loginPopup').popup('close');
                //$("#loginPopup p").html("Connecting...");
                $("#loginPopup p").html(globalizationApp.getLanguageValue("popupConnecting"));
            }, 3000);
        }
        
    });
}

ls.local_login = function(usr, pwd){
    if ((usr == "") || (pwd == "")){
        //$("#loginPopup p").html("The username and/or password fields cannot be empty.");
        $("#loginPopup p").html(globalizationApp.getLanguageValue("popupEmptyLogin"));
        setTimeout(function () {
            $('#loginPopup').popup('close');
            //$("#loginPopup p").html("Connecting...");
            $("#loginPopup p").html(globalizationApp.getLanguageValue("popupConnecting"));
        }, 4000);
        return;
    }
    console.log("login local: " + usr + " - " + pwd);
    gapi.client.user.login({"username":usr,"password": pwd}).execute(function(resp) {
        if (!resp.code) {
            sessionStorage.setItem('id', resp.id);
            sessionStorage.setItem('token', resp.token);
            localStorage.setItem("tkn", resp.token);
            localStorage.setItem("user_id", resp.id);
            $('#loginPopup').popup('close');
            window.location=platformFile;
        }else{
            //$("#loginPopup p").html("There was an error during the login process." + resp.message);
            $("#loginPopup p").html(globalizationApp.getLanguageValue("popupErrorLogin") + resp.message);
            setTimeout(function () {
                $('#loginPopup').popup('close');
                //$("#loginPopup p").html("Connecting...");
                $("#loginPopup p").html(globalizationApp.getLanguageValue("popupConnecting"));
            }, 3000);
        }
        
    });
}

ls.local_create = function(usr, pwd){
    if ((usr == "") || (pwd == "")){
        //$("#loginPopup p").html("The username and/or password fields cannot be empty.");
        $("#loginPopup p").html(globalizationApp.getLanguageValue("popupEmptyLogin"));
        setTimeout(function () {
            $('#loginPopup').popup('close');
            //$("#loginPopup p").html("Connecting...");
            $("#loginPopup p").html(globalizationApp.getLanguageValue("popupConnecting"));
        }, 4000);
        return;
    }
    console.log("create local: " + usr + " - " + pwd);
    gapi.client.user.create({"username":usr,"password": pwd}).execute(function(resp) {
        if (!resp.code) {
            sessionStorage.setItem('id', resp.id);
            sessionStorage.setItem('token', resp.token);
            localStorage.setItem("tkn", resp.token);
            localStorage.setItem("user_id", resp.id);
            $('#loginPopup').popup('close');
            window.location=platformFile;
        }else{
            //$("#loginPopup p").html("There was an error during the login process." + resp.message);
            $("#loginPopup p").html(globalizationApp.getLanguageValue("popupErrorLogin") + resp.message);
            setTimeout(function () {
                $('#loginPopup').popup('close');
                //$("#loginPopup p").html("Connecting...");
                $("#loginPopup p").html(globalizationApp.getLanguageValue("popupConnecting"));
            }, 3000);
        }
        
    });
}

function onResumeApp(){
    if(navigator.connection.type == Connection.NONE){
        //$("#loginPopup p").html("Lifeshare needs an internet connection to work. Please try again later");
        $("#loginPopup p").html(globalizationApp.getLanguageValue("popupNoInternet"));
        setTimeout(function () {
            $('#loginPopup').popup('open', {positionTo: 'window'});
        }, 100);
        setTimeout(function () {
            $('#loginPopup').popup('close');
            //$("#loginPopup p").html("Connecting...");
            $("#loginPopup p").html(globalizationApp.getLanguageValue("popupConnecting"));
        }, 4000);
    }else{
        if (!(localStorage.getItem("tkn") === null)) {
            window.location=platformFile;
        }else{
            window.location="index.html";    
        }
    }
}

function init() {
    var apiRoot = "https://ls-gae-api.appspot.com/_ah/api";
    var apisToLoad;
    var callback = function() {
        if (--apisToLoad == 0) {
            //We show the login elements when we finish loading the APIs
            $('#btnLocal').show();
            $('#btnNewUser').show();
            $('#btnGoogle').show();
            $('#btnFb').show();
            $('#loginControls').show();
        }
    }
    
    apisToLoad = 1; // must match number of calls to gapi.client.load()
    //gapi.client.load('card', 'v2', callback, apiRoot);
    gapi.client.load('user', 'v2', callback, apiRoot);
}
