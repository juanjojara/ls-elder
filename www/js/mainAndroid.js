$(document).on('deviceready', function() {
    $('#settings_page').hide();
    $('#video_page').hide();
    setTimeout(connectSessions, 5000);
});

$(document).on("pagecreate","#pagedeletefollowers",function(event){
    $(this).on("click", "#followersDeleteList a", function (e) {
        //prevent dialog transition 
        e.preventDefault();
        var sourceId = $(this).closest("li").attr("id");
        var sourceName = $(this).closest("li").attr("user-name");
        var sourcePic = $(this).closest("li").attr("user-pic");
        //set that in #dialogPage's data for later use
        $("#YesNoDialogPage").data("selectedUser", sourceId);
        $("#YesNoDialogPage").data("selectedName", sourceName);
        $("#YesNoDialogPage").data("selectedPic", sourcePic);
        //redirect to dialog
        $.mobile.changePage(this.href, {
            role: "dialog"
        });
    });
});

$(document).on("pagebeforeshow", "#YesNoDialogPage", function () {
    //get the chapter Id from data
    var selectedUser=  $(this).data("selectedUser");
    var selectedName=  $(this).data("selectedName");
    var selectedPic=  $(this).data("selectedPic");
    //do anything you want with it.
    var dlgMsg = globalizationApp.getLanguageValue("dlgMsgRemoveFollower");
    $(this).find('[data-role="content"] p')
        .html(dlgMsg + selectedName);
    $('#picUser').attr("src", selectedPic);

    $('#btnConfirm').on('click', function() {
        followerManagement.removeFollower(selectedUser);
    });
});

$(document).on("pagebeforehide", "#YesNoDialogPage", function () {
    $('#btnConfirm').off();
});

function connectSessions(){
    console.log("Connect to Sessions");
    if (!(localStorage.getItem("followers") === null)) {
        //var followers = JSON.parse(localStorage.getItem("followers"));
        if (followers.length >= 1){
            var xmlhttp = new XMLHttpRequest();
            var sessionURL = "https://opentokrtc.com/lsvideocall" + localStorage.getItem("user_id") + ".json";
            console.log("Follower URL: " + sessionURL);
            xmlhttp.open("GET", sessionURL, false);
            xmlhttp.send();
            var data = JSON.parse( xmlhttp.response );
            sessionPre = null;
            //sessionPre.off();
            sessionPre = TB.initSession(data.apiKey, data.sid);
            sessionPre.off('connectionCreated');
            sessionPre.off('sessionDisconnected');
            sessionPre.off('signal');
            sessionPre.on({
                'connectionCreated': function( event ){
                    console.log("Someone connected to the session");
                },
                'sessionDisconnected': function( event ){
                    console.log("SESSION FINISHED");
                },
                'signal': function( event ){
                    console.log("Signal data: " + event.data);
                    if (event.data.indexOf("_") <= 0){
                        sessionPre.signal({
                            type: "status",
                            data: event.data + "_" + localStorage.getItem("user_id")
                        },
                        function(error) {
                            console.log("SIGNAL CALLBACK");
                            if (error) {
                                console.log("signal error: " + error.message);
                            } else {
                                console.log("Signal Sent");
                            }
                        });
                        localStorage.setItem("followerIdVC", event.data);
                        sessionPre.off('connectionCreated');
                        sessionPre.off('sessionDisconnected');
                        sessionPre.off('signal');
            
                        sessionPre.disconnect();
                        //window.location = "lsvideo.html";
                        setTimeout(function() {
                            $('#video_page').show();
                            $('#card_page').hide();
                            gapi.client.user.set_status({"status": "busy"}).execute(function(resp) {
                                if (!resp.code) {
                                    console.log("Status changed to busy");
                                }else{
                                    console.log("ERROR: Status not changed. " + resp.message);
                                }
                            });
                            IncomingCall();                    
                        }, 2000);
                    }
                }
            });
            sessionPre.connect(data.token, function(){
                console.log("Waiting for a video call");
                gapi.client.user.set_status({"status": "online"}).execute(function(resp) {
                    if (!resp.code) {
                        console.log("Status changed to online");
                    }else{
                        console.log("ERROR: Status not changed. " + resp.message);
                    }
                });
            });
        }
    }
}

function init() {
    var apiRoot = "https://ls-gae-api.appspot.com/_ah/api";
    var apisToLoad;
    var user_id = localStorage.getItem("user_id");
    var callback = function () {
        if (--apisToLoad == 0) {
            var id = sessionStorage.getItem('id');
            var token = localStorage.getItem('tkn');

            gapi.auth.setToken({
                access_token: token
            });
            globalizationApp.globalizationInit(function(){
                //Moved here because globalization is not loaded when it is called and it gives an error
                //Show message for adding followers
                var greetMessage = globalizationApp.getLanguageValue("popupMsgWelcome");
                /* HIDDEN IN HTML
                $("#row-followers").on("tap", function () {
                    navigator.notification.alert(greetMessage, null, globalizationApp.getLanguageValue("popupTitleFollowers"), globalizationApp.getLanguageValue("popupButtonDone"));
                });
                */
                var user_id = localStorage.getItem("user_id");
                if (localStorage.getItem(user_id) === null) {
                    localStorage.setItem(user_id, 1);
                    navigator.notification.alert(greetMessage, null, globalizationApp.getLanguageValue("popupTitleFollowers"), globalizationApp.getLanguageValue("popupButtonDone"));
                }                
            });

            window.plugins.insomnia.keepAwake();
//            call the update cards at the start
            ls.updateCards(true);

            //SHOW - HIDE MENU BAR
            $("#card").on("tap", function () {
                if ($("#menuHeader").is(':hidden')){
                    $("#menuHeader").slideDown(1000);
                    setTimeout(function(){
                        $("#menuHeader").slideUp(1000);    
                    }, 10000);
                }else{
                    $("#menuHeader").slideUp(1000);
                }
            });
            
            $('#btnLogout').on('click', function () {
                window.navigator.notification.confirm(
                    globalizationApp.getLanguageValue("dlgConfirmExit"),
                    function (confirmButton) {
                        if (confirmButton == 1) {
                            gapi.client.user.set_status({"status": "offline"}).execute(function(resp) {
                                if (!resp.code) {
                                    console.log("Status changed to offline");
                                }
                            });

                            localStorage.removeItem("tkn");
                            window.plugins.insomnia.allowSleepAgain();
                            window.location = "index.html";
                        }
                    },
                    globalizationApp.getLanguageValue("dlgTitleExit"),
                    [globalizationApp.getLanguageValue("yes"), globalizationApp.getLanguageValue("no")]
                );
            });

            $('#btnSettings').on('click', function () {
                $('#settings_page').show();
                $('#card_page').hide();
            });
            $('#btnDone').on('click', function () {
                if ($('#autoAnswerTrue').is(':checked')) { 
                    localStorage.setItem("autoAnswer", "true");
                } else { 
                    localStorage.removeItem("autoAnswer");
                }
                
                if (!(localStorage.getItem("deletedUser") === null)) {
                    localStorage.removeItem("deletedUser");
                    window.location="mainAndroid.html";
                }else{
                    $('#card_page').show();
                    $('#settings_page').hide();
                }
            });
            document.addEventListener("resume", onResumeApp, false);

            //Automatic Answer Switch
            if ((localStorage.getItem("autoAnswer") === null)) {
                $('#autoAnswerTrue').attr("checked",false).checkboxradio("refresh");
                $('#autoAnswerFalse').attr("checked",true).checkboxradio("refresh");
            }else{
                $('#autoAnswerFalse').attr("checked",false).checkboxradio("refresh");
                $('#autoAnswerTrue').attr("checked",true).checkboxradio("refresh");
            }
        }
    }

    apisToLoad = 2; // must match number of calls to gapi.client.load()
    gapi.client.load('card', 'v2', callback, apiRoot);
    gapi.client.load('user', 'v2', callback, apiRoot);
}

function onResumeApp(){
    window.location = "mainAndroid.html";
}

function IncomingCall(){
    var followersVC = JSON.parse(localStorage.getItem("followers"));
    var selectedFId =  localStorage.getItem("followerIdVC");
    
    //Get the name of the person that is calling
    var followerName = "";
    var followerPic = "";
    for (var i = 0; i < followersVC.length; i++) {
        var curFollower = followersVC[i];
        if (curFollower.id == selectedFId){
            followerName = curFollower.username;
            followerPic = curFollower.picture;
        }
    }

    //Fix the video layout
    video_layout = JSON.parse('{}');
    var screenWidth = $(window).width();
    //VIDEO EJS
    console.log("W: " + $(window).width() + " - H: " + $(window).height());
    video_layout.myVideoTop = $(window).height()*0.8-70;
    video_layout.myVideoLeft = $(window).width()*0.8;
    video_layout.callerVideoTop = 80;
    video_layout.callerVideoLeft = 0;
    video_layout.bottomBarTop = $(window).height() - 70;
//    video_layout.bottomIconTop = $(window).height() - 60;
    video_layout.callingUserTop = $(window).height()*0.3;
    video_layout.callingUserLeft = $(window).width()*0.3;
    video_layout.followerName = followerName;
    if (screenWidth <= 350){
        video_layout.fontSize = 15;
        video_layout.buttonWidth = screenWidth - 20;
        video_layout.iconWidth = screenWidth*0.1;
        video_layout.iconHeight = screenWidth*0.05;
        video_layout.labelLeft = screenWidth*0.15;
        video_layout.labelWidth = screenWidth*0.85;
    }else{
        video_layout.fontSize = 30;
        video_layout.buttonWidth = 350;
        video_layout.iconWidth = 70;
        video_layout.iconHeight = 35;
        video_layout.labelLeft = screenWidth*0.2;;
        video_layout.labelWidth = screenWidth*0.80;
    }
    var result = new EJS({url: 'ejs/videoTablet.ejs'}).render(video_layout);
    $('#video').html(result);
    //Resize
    $('.fullWidth').css('width', $(window).width() + 'px');

    //Set Picture of the user calling
    $('#picUserCalling').attr("src", followerPic);

    //In case of any error, we go back to slideshow after 60 seconds of no response
    localStorage.setItem("callError", "true");
    setTimeout(backToMain, 1000*60);
        
    //Set text information of the user that is calling
    $('#userCallLabel').html(followerName + " " + globalizationApp.getLanguageValue("callingUserMessage"));
    $('#videoCallLabel').html(globalizationApp.getLanguageValue("videoCallFooterLabel") + followerName);

    try{
        //We GET the session details
        var xmlhttp = new XMLHttpRequest();
        var sessionCallURL = "https://opentokrtc.com/" + selectedFId + "_" + localStorage.getItem("user_id") + ".json";
        console.log("Follower URL: " + sessionCallURL);
        xmlhttp.open("GET", sessionCallURL, false);
        xmlhttp.send();
        var dataCall = JSON.parse( xmlhttp.response );

        //Create the session object
        sessionVC = TB.initSession(dataCall.apiKey, dataCall.sid);
        var myVideoWidth = $(window).width()*0.18;
        var myVideoHeight = $(window).height()*0.18;
        publisherVC = TB.initPublisher(dataCall.apiKey,'myPublisherDiv', {width:myVideoWidth, height:myVideoHeight} );

        //Buttoj to Finish call. Declared here so the publisher gets closed after clicking it
        $('#btnEndVideoCall').on('click', function () {
            finishCall();
        });
        sessionVC.on({
            'streamCreated': function( event ){
                localStorage.removeItem("waitingVideo");        //cancel the timeout exit
                navigator.notification.beep(3);
                if ((localStorage.getItem("autoAnswer") === null)) {
                    localStorage.setItem("ringing", "true");
                    ringingCall();

                    window.navigator.notification.confirm(
                        followerName + " " + globalizationApp.getLanguageValue("callingUserMessage"),
                        function(confirmButton){
                            localStorage.removeItem("ringing");        //cancel the ringing tone
                            if (confirmButton == 1){
                                startCall(event);
                            } else if (confirmButton == 2){
                                if (device.platform != "Android"){
                                    finishCall();
                                }else{
                                    startCall(event);
                                    setTimeout(finishCall,5000);
                                }
                            }
                        }, 
                        globalizationApp.getLanguageValue("callingUserTitle"), 
                        [globalizationApp.getLanguageValue("yes"),globalizationApp.getLanguageValue("no")]
                    );
                }else{
                    startCall(event);
                }

            },
            'streamDestroyed': function( event ){
                localStorage.setItem("callEndedUnexpectedly", "true");
                setTimeout(callInterrupted, 10000);
            },
            'sessionDisconnected': function( event ){
                localStorage.setItem("callEndedUnexpectedly", "true");
                setTimeout(callInterrupted, 12000);
            },
            'signal': function( event ){
                console.log("Signal data: " + event.data);
                if (event.data == "ciao" + localStorage.getItem("user_id")){
                    EndSession();
                }
            }
        });
        sessionVC.connect(dataCall.token, function(){
            localStorage.removeItem("callError");        //cancel the timeout exit
            //if we do not receive a video stream from the other side we automatically close
            localStorage.setItem("waitingVideo", "true");
            setTimeout(function(){
                if (!(localStorage.getItem("waitingVideo") === null)) {
                    localStorage.removeItem("waitingVideo");
                    finishCall();
                }
            },60000);
        });
    } catch (e) {
        console.log("General ERROR!!!!!!!");
        console.log(e.message);
        //window.location = platformFile;
        $('#video_page').hide();
        $('#card_page').show();
        setTimeout(connectSessions, 5000);
    }
}

function EndSession(){
    console.log("END SESSION 111");
    sessionVC.off('streamCreated');
    sessionVC.off('streamDestroyed');
    sessionVC.off('sessionDisconnected');
    sessionVC.off('signal');
    sessionVC.disconnect();
    console.log("END SESSION 222");
    localStorage.removeItem("callEndedUnexpectedly");        //cancel the timeout exit
    localStorage.removeItem("waitingVideo");

    console.log("END SESSION 444");
    setTimeout(function(){
        console.log("END SESSION 555");
        //window.location = platformFile;
        $('#video_page').hide();
        $('#card_page').show();
    },3000);
    setTimeout(connectSessions, 5000);
}

function startCall(event){
    sessionVC.publish( publisherVC );
    $('#callerInfo').hide();
    var videoWidth = $(window).width();
    var videoHeight = $(window).height()-150;
    subscriberVC = sessionVC.subscribe( event.stream, 'otherPublisherDiv', {width:videoWidth, height:videoHeight} );
}

function finishCall(){
    console.log("FINISH CALL 111");
    sessionVC.signal({
        type: "status",
        data: "ciao" + localStorage.getItem("followerIdVC")
    },
    function(error) {
        console.log("SIGNAL CALLBACK");
        if (error) {
            console.log("signal error: " + error.message);
        } else {
            console.log("Signal Sent");
        }
    });
    console.log("FINISH CALL 222");
    EndSession();
}

function callInterrupted(){
    if (!(localStorage.getItem("callEndedUnexpectedly") === null)) {
        EndSession();
    }
}

function backToMain(){
    if (!(localStorage.getItem("callError") === null)) {
        localStorage.removeItem("callError");
        //window.location = platformFile;
        $('#video_page').hide();
        $('#card_page').show();
        setTimeout(connectSessions, 5000);
    }
}

function ringingCall(){
    if (!(localStorage.getItem("ringing") === null)) {
        navigator.notification.beep(3);
        setTimeout(ringingCall, 6000);
    }
}
