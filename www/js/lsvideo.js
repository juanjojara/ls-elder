var video_layout;
var platformFile;

$(document).on('deviceready', function() {
    if (device.platform != "Android"){
        platformFile = "main.html";
    }else{
        platformFile = "mainAndroid.html";
    }
    
    var followersVC = JSON.parse(localStorage.getItem("followers"));
    var selectedFId =  localStorage.getItem("followerIdVC");
    
    //Get the name of the person that is calling
    var followerName = "";
    var followerPic = "";
    for (var i = 0; i < followersVC.length; i++) {
        var curFollower = followersVC[i];
        if (curFollower.id == selectedFId){
            followerName = curFollower.username;
            followerPic = curFollower.picture;
        }
    }

    video_layout = JSON.parse('{}');
    var screenWidth = $(window).width();
    //VIDEO EJS
    console.log("W: " + $(window).width() + " - H: " + $(window).height());
    video_layout.myVideoTop = $(window).height()*0.8-70;
    video_layout.myVideoLeft = $(window).width()*0.8;
    video_layout.callerVideoTop = 80;
    video_layout.callerVideoLeft = 0;
    video_layout.bottomBarTop = $(window).height() - 70;
//    video_layout.bottomIconTop = $(window).height() - 60;
    video_layout.callingUserTop = $(window).height()*0.3;
    video_layout.callingUserLeft = $(window).width()*0.3;
    video_layout.followerName = followerName;
    if (screenWidth <= 350){
        video_layout.fontSize = 15;
        video_layout.buttonWidth = screenWidth - 20;
        video_layout.iconWidth = screenWidth*0.1;
        video_layout.iconHeight = screenWidth*0.05;
        video_layout.labelLeft = screenWidth*0.15;
        video_layout.labelWidth = screenWidth*0.85;
    }else{
        video_layout.fontSize = 30;
        video_layout.buttonWidth = 350;
        video_layout.iconWidth = 70;
        video_layout.iconHeight = 35;
        video_layout.labelLeft = screenWidth*0.2;;
        video_layout.labelWidth = screenWidth*0.80;
    }
    var result = new EJS({url: 'ejs/videoTablet.ejs'}).render(video_layout);
    $('#video').html(result);
    //Resize
    $('.fullWidth').css('width', $(window).width() + 'px');
    

    //Set Picture of the user calling
    $('#picUserCalling').attr("src", followerPic);
//    $('#picUserCallingBar').attr("src", followerPic);

    //In case of any error, we go back to slideshow after 60 seconds of no response
    localStorage.setItem("callError", "true");
    setTimeout(backToMain, 1000*60);
        
    //Initialize the globalization plugin and translate the texts
    globalizationApp.globalizationInit(function(){
        //Set text information of the user that is calling
        $('#userCallLabel').html(followerName + " " + globalizationApp.getLanguageValue("callingUserMessage"));
        $('#videoCallLabel').html(globalizationApp.getLanguageValue("videoCallFooterLabel") + followerName);
    });

    try{
        //We GET the session details
        var xmlhttp = new XMLHttpRequest();
        var sessionURL = "https://opentokrtc.com/" + selectedFId + "_" + localStorage.getItem("user_id") + ".json";
        console.log("Follower URL: " + sessionURL);
        xmlhttp.open("GET", sessionURL, false);
        xmlhttp.send();
        var data = JSON.parse( xmlhttp.response );

        //Create the session object
        sessionVC = TB.initSession(data.apiKey, data.sid);
        var myVideoWidth = $(window).width()*0.18;
        var myVideoHeight = $(window).height()*0.18;
        publisherVC = TB.initPublisher(data.apiKey,'myPublisherDiv', {width:myVideoWidth, height:myVideoHeight} );

        //Buttoj to Finish call. Declared here so the publisher gets closed after clicking it
        $('#btnEndVideoCall').on('click', function () {
            finishCall();
        });
        sessionVC.on({
            'streamCreated': function( event ){
                localStorage.removeItem("waitingVideo");        //cancel the timeout exit
                navigator.notification.beep(3);
                cardUtils.setVideoReceived(localStorage.getItem("user_id"));
                if ((localStorage.getItem("autoAnswer") === null)) {
                    localStorage.setItem("ringing", "true");
                    ringingCall();

                    window.navigator.notification.confirm(
                        followerName + " " + globalizationApp.getLanguageValue("callingUserMessage"),
                        function(confirmButton){
                            localStorage.removeItem("ringing");        //cancel the ringing tone
                            if (confirmButton == 1){
                                startCall(event);
                                cardUtils.setVideoAnswered(localStorage.getItem("user_id"));
                            } else if (confirmButton == 2){
                                cardUtils.setVideoRejected(localStorage.getItem("user_id"));
                                if (device.platform != "Android"){
                                    finishCall();
                                }else{
                                    startCall(event);
                                    setTimeout(finishCall,5000);
                                }
                            }
                        }, 
                        globalizationApp.getLanguageValue("callingUserTitle"), 
                        [globalizationApp.getLanguageValue("yes"),globalizationApp.getLanguageValue("no")]
                    );
                }else{
                    startCall(event);
                    cardUtils.setVideoAnswered(localStorage.getItem("user_id"));
                }

            },
            'streamDestroyed': function( event ){
                localStorage.setItem("callEndedUnexpectedly", "true");
                setTimeout(callInterrupted, 10000);
            },
            'sessionDisconnected': function( event ){
                localStorage.setItem("callEndedUnexpectedly", "true");
                setTimeout(callInterrupted, 12000);
            },
            'signal': function( event ){
                console.log("Signal data: " + event.data);
                if (event.data == "ciao" + localStorage.getItem("user_id")){
                    EndSession();
                }
            }
        });
        sessionVC.connect(data.token, function(){
            localStorage.removeItem("callError");        //cancel the timeout exit
            //if we do not receive a video stream from the other side we automatically close
            localStorage.setItem("waitingVideo", "true");
            setTimeout(function(){
                if (!(localStorage.getItem("waitingVideo") === null)) {
                    localStorage.removeItem("waitingVideo");
                    finishCall();
                }
            },60000);
        });
    } catch (e) {
        console.log("General ERROR!!!!!!!");
        console.log(e.message);
        window.location = platformFile;
    }
    
});


function EndSession(){
    console.log("END SESSION 111");
    sessionVC.off();
    sessionVC.disconnect();
    console.log("END SESSION 222");
    localStorage.removeItem("callEndedUnexpectedly");        //cancel the timeout exit
    localStorage.removeItem("waitingVideo");
    if (device.platform != "Android"){
        console.log("END SESSION 333");
        window.location = platformFile;
    } else {
        console.log("END SESSION 444");
        setTimeout(function(){
            console.log("END SESSION 555");
            window.location = platformFile;
        },3000);
    }
}

function startCall(event){
    sessionVC.publish( publisherVC );
    $('#callerInfo').hide();
    var videoWidth = $(window).width();
    var videoHeight = $(window).height()-150;
    subscriberVC = sessionVC.subscribe( event.stream, 'otherPublisherDiv', {width:videoWidth, height:videoHeight} );
}

function finishCall(){
    console.log("FINISH CALL 111");
    sessionVC.signal({
        type: "status",
        data: "ciao" + localStorage.getItem("followerIdVC")
    },
    function(error) {
        console.log("SIGNAL CALLBACK");
        if (error) {
            console.log("signal error: " + error.message);
        } else {
            console.log("Signal Sent");
        }
    });
    console.log("FINISH CALL 222");
    if (device.platform != "Android"){
        console.log("FINISH CALL 222.5555");
        publisherVC.destroy();
    }
    console.log("FINISH CALL 333");
    EndSession();
}

function finishCallAndroid(){
    console.log("ANDROID - FINISH CALL 111");
    sessionVC.signal({
        type: "status",
        data: "ciao" + localStorage.getItem("followerIdVC")
    },
    function(error) {
        console.log("SIGNAL CALLBACK");
        if (error) {
            console.log("signal error: " + error.message);
        } else {
            console.log("Signal Sent");
        }
    });
    console.log("ANDROID - FINISH CALL 222");
    console.log("ANDROID - END SESSION 111");
    sessionVC.off();
    //sessionVC.disconnect();
    console.log("ANDROID - END SESSION 222");
    localStorage.removeItem("callEndedUnexpectedly");        //cancel the timeout exit
    localStorage.removeItem("waitingVideo");
    console.log("ANDROID - END SESSION 333");
    setTimeout(function(){
        console.log("ANDROID - END SESSION 444");
        window.location = platformFile;
    },3000);
}

function callInterrupted(){
    if (!(localStorage.getItem("callEndedUnexpectedly") === null)) {
        EndSession();
    }
}

function backToMain(){
    if (!(localStorage.getItem("callError") === null)) {
        localStorage.removeItem("callError");
        window.location = platformFile;
    }
}

function ringingCall(){
    if (!(localStorage.getItem("ringing") === null)) {
        navigator.notification.beep(3);
        setTimeout(ringingCall, 6000);
    }
}

function init() {
    var apiRoot = "https://ls-gae-api.appspot.com/_ah/api";
    var apisToLoad;
    var callback = function () {
        if (--apisToLoad == 0) {
            var token = localStorage.getItem('tkn');
            gapi.auth.setToken({
                access_token: token
            });
            gapi.client.user.set_status({"status": "busy"}).execute(function(resp) {
                if (!resp.code) {
                    console.log("Status changed to busy");
                }else{
                    console.log("ERROR: Status not changed. " + resp.message);
                }
            });
        }
    }
    document.addEventListener("resume", onResumeApp, false);

    apisToLoad = 1; // must match number of calls to gapi.client.load()
    gapi.client.load('user', 'v2', callback, apiRoot);
}