var followerManagement = followerManagement || {};

followerManagement.removeFollower = function(userId){
    gapi.client.user.removeuserId({"userId":userId}).execute(function(resp) {
        console.log(resp);
        if (!resp.code) {
        	console.log("remove YES" + userId);
        	var itemId = "#" + userId;
        	$(itemId).remove();
            localStorage.setItem("deletedUser", "true");
        }else{
        	console.log("remove NO");
            $("#error").html(resp.message);
        }
    });
}

var ajax = {  
    PopulateListView:function(action){
        //var lv_followers = followers;
        var lv_followers = JSON.parse(localStorage.getItem("followers"));
    	lv_followers.sort(compare);
        if (action == "delete"){
            $('#followersDeleteList').empty();
            $.each(lv_followers, function(i, row) {
                $('#followersDeleteList').append('<li data-icon="minus" id="' + row.id + '" user-name="' + row.username + '" user-pic="' + row.picture + '"><a href="#YesNoDialogPage" data-id="' + row.id + '"><img src="' + row.picture + '">' + row.username + '</a></li>');
            });
            $('#followersDeleteList').listview('refresh');
        }        
    }
}

function compare(a,b) {
  if (a.username.toUpperCase() < b.username.toUpperCase())
     return -1;
  if (a.username.toUpperCase() > b.username.toUpperCase())
    return 1;
  return 0;
}
