var cardUtils = cardUtils || {};

cardUtils.weatherImage = function (weather) {
    var weatherImg;
    switch (weather) {
        case "Breezy":
            weatherImg = "breezy.png";
            break;
        case "Breezy and Dry":
            weatherImg = "breezy-dry.png";
            break;
        case "Breezy and Mostly Cloudy":
            weatherImg = "breezy-mostly-cloudy.png";
            break;
        case "Breezy and Partly Cloudy":
            weatherImg = "breezy-partly-cloudy.png";
            break;
        case "Clear":
            weatherImg = "Clear.png";
            break;
        case "Drizzle":
            weatherImg = "drizzle.png";
            break;
        case "Drizzle and Breezy":
            weatherImg = "drizzle-breezy.png";
            break;
        case "Drizzle and Windy":
            weatherImg = "drizzle-windy.png";
            break;
        case "Dry":
            weatherImg = "dry.png";
            break;
        case "Dry and Mostly Cloudy":
            weatherImg = "dry-mostly-cloudy.png";
            break;
        case "Dry and Overcast":
            weatherImg = "dry-overcast.png";
            break;
        case "Dry and Partly Cloudy":
            weatherImg = "dry-partly-cloudy.png";
            break;
        case "Flurries":
            weatherImg = "flurries.png";
            break;
        case "Foggy":
            weatherImg = "foggy.png";
            break;
        case "Heavy Rain":
            weatherImg = "heavy-rain.png";
            break;
        case "Humid":
            weatherImg = "humid.png";
            break;
        case "Humid and Mostly Cloudy":
            weatherImg = "humid-mostly-cloudy.png";
            break;
        case "Humid and Overcast":
            weatherImg = "humid-overcast.png";
            break;
        case "Humid and Partly Cloudy":
            weatherImg = "humid-partly-cloudy.png";
            break;
        case "Light Rain":
            weatherImg = "light-rain.png";
            break;
        case "Light Rain and Breezy":
            weatherImg = "light-rain-breezy.png";
            break;
        case "Light Rain and Windy":
            weatherImg = "light-rain-windy.png";
            break;
        case "Light Snow":
            weatherImg = "light-snow.png";
            break;
        case "Mostly Cloudy":
            weatherImg = "mostly-cloudy.png";
            break;
        case "Overcast":
            weatherImg = "overcast.png";
            break;
        case "Partly Cloudy":
            weatherImg = "partly-cloudy.png";
            break;
        case "Rain":
            weatherImg = "rain.png";
            break;
        case "Rain and Breezy":
            weatherImg = "rain-breezy.png";
            break;
        case "Snow":
            weatherImg = "snow.png";
            break;
        case "Windy and Mostly Cloudy":
            weatherImg = "windy-mostly-cloudy.png";
            break;
        case "Windy and Partly Cloudy":
            weatherImg = "windy-partly-cloudy.png";
            break;
        default:
            weatherImg = "breezy-dry.png";
    }
    return weatherImg;
}

// dateCardString is a date in string format
cardUtils.dateDiffInDays = function(dateCardString) {
    var timeFromCard = "";
    var _MS_PER_DAY = 1000 * 60 * 60 * 24;

    var yearCard = dateCardString.substring(0,4);
    var monthCard = parseInt(dateCardString.substring(5,7))-1;
    var dayCard = dateCardString.substring(8,10);
    var hourCard = dateCardString.substring(11,13);
    var minuteCard = dateCardString.substring(14,16);
    var secondCard = dateCardString.substring(17,19);
    var milisCard = dateCardString.substring(20);

    var a = new Date(yearCard, monthCard, dayCard, hourCard, minuteCard, secondCard, milisCard); 
    var b = new Date();

    // Discard the time and time-zone information.
    var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
    var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

    var daysAgo = Math.floor((utc2 - utc1) / _MS_PER_DAY);

    if (daysAgo <= 31){
        if (daysAgo < 1){
            //Today
            timeFromCard = globalizationApp.getLanguageValue("today");
        } else {
            if (daysAgo == 1){
                //1 day
                timeFromCard = daysAgo + " " + globalizationApp.getLanguageValue("1day");
            } else {
                //more than a day
                timeFromCard = daysAgo + " " + globalizationApp.getLanguageValue("xday");
            }
        }
    }else{
        var monthsAgo = Math.floor(daysAgo / 30);
        if (monthsAgo <= 1){
            //1 month
            timeFromCard = monthsAgo + " " + globalizationApp.getLanguageValue("1month");
        } else {
            //more than a month
            timeFromCard = monthsAgo + " " + globalizationApp.getLanguageValue("xmonth");
        }
    }

    return timeFromCard;
}

cardUtils.setTap = function(userId) {
    console.log("TAP");
    if (localStorage.getItem(userId + "_tap") === null) {
        localStorage.setItem(userId + "_tap", 1);
    }else{
        localStorage.setItem(userId + "_tap", parseInt(localStorage.getItem(userId + "_tap")) + 1);
    }
}

cardUtils.setSlideForward = function(userId) {
    console.log("FF >>");
    if (localStorage.getItem(userId + "_slideForward") === null) {
        localStorage.setItem(userId + "_slideForward", 1);
    }else{
        localStorage.setItem(userId + "_slideForward", parseInt(localStorage.getItem(userId + "_slideForward")) + 1);
    }
}

cardUtils.setSlideBackward = function(userId) {
    console.log("BB <<");
    if (localStorage.getItem(userId + "_slideBackward") === null) {
        localStorage.setItem(userId + "_slideBackward", 1);
    }else{
        localStorage.setItem(userId + "_slideBackward", parseInt(localStorage.getItem(userId + "_slideBackward")) + 1);
    }
}

cardUtils.setVideoReceived = function(userId) {
    console.log("VIDEO INC");
    if (localStorage.getItem(userId + "_videoReceived") === null) {
        localStorage.setItem(userId + "_videoReceived", 1);
    }else{
        localStorage.setItem(userId + "_videoReceived", parseInt(localStorage.getItem(userId + "_videoReceived")) + 1);
    }
}

cardUtils.setVideoAnswered = function(userId) {
    console.log("VIDEO ANS");
    if (localStorage.getItem(userId + "_videoAnswered") === null) {
        localStorage.setItem(userId + "_videoAnswered", 1);
    }else{
        localStorage.setItem(userId + "_videoAnswered", parseInt(localStorage.getItem(userId + "_videoAnswered")) + 1);
    }
}

cardUtils.setVideoRejected = function(userId) {
    console.log("VIDEO REJ");
    if (localStorage.getItem(userId + "_videoRejected") === null) {
        localStorage.setItem(userId + "_videoRejected", 1);
    }else{
        localStorage.setItem(userId + "_videoRejected", parseInt(localStorage.getItem(userId + "_videoRejected")) + 1);
    }
}