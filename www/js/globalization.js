var globalizationApp = {
    languageSpecificObjectGlobal: null,

    globalizationInit: function(callback){
        var cLANGUAGE = null;
        navigator.globalization.getPreferredLanguage(
            //Get Language from Settings
            function (locale) {
                cLANGUAGE = locale.value;
                globalizationApp.languageControls(cLANGUAGE, callback);
            },
            //On Failure set language to english
            function () {cLANGUAGE = "en";}
        );
    },
     
    //Function to make network call according to language on load
    languageControls: function(language, callback){
        var languageSpecificObject = null;
        var languageSpecificURL = "";
        var spanishLanguageSpecificURL = "i18n/es/strings.json";
        var englishLanguageSpecificURL = "i18n/en/strings.json";
        var russianLanguageSpecificURL = "i18n/ru/strings.json";
        var italianLanguageSpecificURL = "i18n/it/strings.json";
        var polishLanguageSpecificURL = "i18n/pl/strings.json";
        
        console.log("Language: " + language.toString());
        if((language.toString() == "es") || (language.toString() == "español") || (language.toString().indexOf("es") != -1)){
            languageSpecificURL = spanishLanguageSpecificURL;
        } else if((language.toString() == "ru") || (language.toString() == "Русский") || (language.toString().indexOf("ru") != -1)){
            languageSpecificURL = russianLanguageSpecificURL;
        } else if((language.toString() == "it") || (language.toString() == "italiano") || (language.toString().indexOf("it") != -1)){
            languageSpecificURL = italianLanguageSpecificURL;
        } else if((language.toString() == "pl") || (language.toString() == "polski") || (language.toString().indexOf("pl") != -1)){
            languageSpecificURL = polishLanguageSpecificURL;
        }
        else{
            //Default English
            languageSpecificURL = englishLanguageSpecificURL;
        }
        //Make an ajax call to strings.json files
        globalizationApp.onNetworkCall(languageSpecificURL,function(msg){
            languageSpecificObject = JSON.parse(msg);
            globalizationApp.languageSpecificObjectGlobal = languageSpecificObject;
            $(".languagespecificHTML").each(function(){
                $(this).html(languageSpecificObject.languageSpecifications[0][$(this).data("text")]);
            });
            $(".languageSpecificPlaceholder").each(function(){
                $(this).attr("placeholder",languageSpecificObject.languageSpecifications[0][$(this).data("text")]);
            });
                    $(".languageSpecificValue").each(function(){
                $(this).attr("value",languageSpecificObject.languageSpecifications[0][$(this).data("text")]);
            });

            if (callback !== null){
                callback();
            }
        });
    },
    //Function to get specific value with unique key
    getLanguageValue: function(key){
        value = globalizationApp.languageSpecificObjectGlobal.languageSpecifications[0][key];
        return value;
    },
    //Network Call
    onNetworkCall: function(urlToHit,successCallback){
        $.ajax({
           type: "POST",
           url: urlToHit,
           timeout: 30000 ,
           }).done(function( msg ) {
               successCallback(msg);
                   }).fail(function(jqXHR, textStatus, errorThrown){
                       alert("Internal Server Error");
                   });
    }
}
