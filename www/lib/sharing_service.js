var bgGeo = window.plugins.backgroundGeoLocation;
var curLocation;

var lsService = {
	configureBackgroundGeoLocation: function() {
		console.log("Configuring GeoLocation INSIDE");
        // Your app must execute AT LEAST ONE call for the current position via standard Cordova geolocation,
        //  in order to prompt the user for Location permission.
        window.navigator.geolocation.getCurrentPosition(function(location) {
            console.log('Location from Phonegap');
            reverseLocGoogle(location.latitudue, location.longitude);
        });

        //var bgGeo = window.plugins.backgroundGeoLocation;

        /**
        * This would be your own callback for Ajax-requests after POSTing background geolocation to your server.
        */
        var yourAjaxCallback = function(response) {
            ////
            // IMPORTANT:  You must execute the #finish method here to inform the native plugin that you're finished,
            //  and the background-task may be completed.  You must do this regardless if your HTTP request is successful or not.
            // IF YOU DON'T, ios will CRASH YOUR APP for spending too much time in the background.
            //
            //
            bgGeo.finish();
        };

        /**
        * This callback will be executed every time a geolocation is recorded in the background.
        */
        var callbackFn = function(location) {
            console.log('[js] BackgroundGeoLocation callback:  ' + location.latitudue + ',' + location.longitude);

            // Do your HTTP request here to POST location to your server.
            reverseLocGoogle(location.latitudue, location.longitude);
            
            yourAjaxCallback.call(this);
        };

        var failureFn = function(error) {
            console.log('BackgroundGeoLocation error');
        }
        
        // BackgroundGeoLocation is highly configurable.
        bgGeo.configure(callbackFn, failureFn, {
            url: 'http://only.for.android.com/update_location.json', // <-- only required for Android; ios allows javascript callbacks for your http
            headers: {
            	Authorization: '242c3744553868004218448df16f6ea67218f533'
            }
            params: {                                               // HTTP POST params sent to your server when persisting locations.
                info: 'user_secret_auth_token',
                lat: 'bar',
                lon: '',
                location: ''
            },
            desiredAccuracy: 10,
            stationaryRadius: 20,
            distanceFilter: 30,
            debug: true, // <-- enable this hear sounds for background-geolocation life-cycle.
            notificationTitle: 'Background tracking', // <-- android only, customize the title of the notification
            notificationText: 'ENABLED' // <-- android only, customize the text of the notification
        });

        // Turn ON the background-geolocation system.  The user will be tracked whenever they suspend the app.
        bgGeo.start();

        // If you wish to turn OFF background-tracking, call the #stop method.
        // bgGeo.stop()
    },
    start: function() {
		bgGeo.start();
	},
    stop: function() {
		bgGeo.stop();
	}
};

function reverseLocGoogle(lat, lng){
    var url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + lng + "&sensor=true";
    console.log("URL: " + url);
    $.get( url, function( data ) {
      alert( "Data reverse geo coded: " + data );
      console.log("RESP: " + data);
    });

    /*var client = Ti.Network.createHTTPClient({
        // function called when the response data is available
        onload : function(e) {
            var addr = (JSON.parse(this.responseText)).results[0];
            var addr_comp = addr.address_components;
            var revCity;
            var revRegion;
            var revCountry;
            for (i=0; i<addr_comp.length;i++){
                if ( addr_comp[i].types[0] == "administrative_area_level_1" || addr_comp[i].types[0] == "administrative_area_level_2" ) {
                    revRegion = addr_comp[i].long_name;
                } else if ( addr_comp[i].types[0] == "country") {
                    revCountry = addr_comp[i].long_name;
                } else if ( addr_comp[i].types[0] == "administrative_area_level_3" || addr_comp[i].types[0] == "locality") {
                    revCity = addr_comp[i].long_name;
                }
            }
            curLocation={city:revCity,country:revCountry,region:revRegion};
            savePosition();
        },
        // function called when an error occurs, including a timeout
        onerror : function(e) {
            Ti.API.info("ERROR during Google reverse geocoding!"); //INFO FOR DEBUG
            Ti.API.debug(e.error); //INFO FOR DEBUG
        },
        timeout : 5000  // in milliseconds
    });
    // Prepare the connection.
    client.open("GET", url);
    client.setRequestHeader('Content-Type', 'application/json');
    // Send the request.
    client.send();
    */
};

function savePosition(Latitude, Longitude){
		if (localStorage.getItem("tkn") === null)) {
			console.log("No TOKEN");
		}
		if (localStorage.getItem("location_setting") === null)) {
			console.log("No LOC SET");
		}
		if (localStorage.getItem("sharing_setting") === null)) {
			console.log("No SHA SET");
		}
		var currentdate = new Date(); 
		var datetime = "Last Sync: " + currentdate.getDate() + "/"
	                + (currentdate.getMonth()+1)  + "/" 
	                + currentdate.getFullYear() + " @ "  
	                + currentdate.getHours() + ":"  
	                + currentdate.getMinutes() + ":" 
	                + currentdate.getSeconds();

		var Location = 'unavailable';
		var userloc_setting = localStorage.getItem("location_setting");
		if (!(typeof curLocation.city === "undefined") && (location_level(userloc_setting) <= 0)){
			Location = curLocation.city;
			if (!(typeof curLocation.region === "undefined")){
				Location = Location + ', ' + curLocation.region;
			}
			if (!(typeof curLocation.country === "undefined")){
				Location = Location + ', ' + curLocation.country;
			}
		}else if (!(typeof curLocation.region === "undefined") && (location_level(userloc_setting) <= 1)){
			Location = curLocation.region;
			if (!(typeof curLocation.country === "undefined")){
				Location = Location + ', ' + curLocation.country;
			}
		}else if (!(typeof curLocation.country === "undefined") && (location_level(userloc_setting) <= 2)){
			Location = curLocation.country;
		}
		
		var Info = 'is_at';
		if (currentdate.getHours<=6){
			Info = 'is_sleeping';
		} else if (currentdate.getHours>= 12 && currentdate.getHours<=14){
			Info = 'is_having_lunch';
		}

		var usrToken = localStorage.getItem("tkn");
		var locationInfo = {info: Info, lat: Latitude, lon: Longitude, location: Location};

		gapi.client.card.create({info: Info, lat: Latitude, lon: Longitude, location: Location}).execute(function(resp) {
	        if (!resp.code) {
	        	console.log("CARD YES");
	        }else{
	        	console.log("CARD NO");
	        }
	        
	    });
};

function location_level(loc_level){
	var ret_level;
	switch(loc_level) {
	    case "city":
	        ret_level=0;
	        break;
	    case "region":
	        ret_level=1;
	        break;
	    case "country":
	        ret_level=2;
	        break;
	    default:
	        ret_level=3;
	}
	return ret_level;
};