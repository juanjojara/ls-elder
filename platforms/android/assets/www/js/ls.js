var token = ""
var ls = ls || {};
var users = []
var index = 0;
var followers_to_show = 4;
var resize_list = {};
resize_list[1] = {'r': 1, 'c': 1};
resize_list[4] = {'r': 2, 'c': 2};
resize_list[6] = {'r': 2, 'c': 3};
resize_list[12] = {'r': 3, 'c': 4};
resize_list[20] = {'r': 4, 'c': 5};
var len_pics = 1;
var number_of_pic = 1;
var card;
var timer = 40000;
var card_size = 0;
var number_of_card = 0;                 //Indicates the current card number showed for the current follower (pointed by index)
var change_follower = false;            //Indicates that if there are no more cards for the current followers then we need to change follower ( -> index + 1)
var followers_without_cards = true;     //Indicates that at the moment any of the followers has any cards, so we will have to go to slideshow mode

ls.resize = function () {
//            do some math to adjust the picture.
//          uses row and column parameters
// play with the parameters here.
    //var sH = $(window).height() - 125;  HEIGHT with space for HEADER
    var sH = $(window).height() - 100;          // 100 is the height of the bottom bar
    if (card_size == 1){
        sH = $(window).height();
        card_size = 0;
    }else{
        card_size = 1;
    }

    $('.fullsize').css('height', sH + 'px');
    //$('#row-followers').css('height', (sH + 75) + 'px');          HIDDEN IN HTML
    $('#photo').css('width', $('.fullsize').width() + 'px');
    var res = resize_list[len_pics];
    if (res == undefined)
        res = resize_list[20];
    r = res['r'];
    c = res['c'];

    //$(".img-card").css('width', ($('#photo').width() / c) - 5);
    $(".img-card").css('width', ($('#photo').width() / c) + 60);
    $('#photo').css('left',-15);
    $('#photo').css('height', sH + 'px');
    //$(".img-card").css('height', ($('#photo').height() / r) - 5);
    $(".img-card").css('height', ($('#photo').height() / r));

    //$('section:not(.header-10-sub):not(.content-11)').css('height', px);xw
}

ls.showFollowers = function () {
    if (!(localStorage.getItem("followers") === null)) {
        //var my_followers = JSON.parse(localStorage.getItem("followers"));
        var result = new EJS({url: 'ejs/followers.ejs'}).render(followers);
        $('#followers').html(result);
    }
}

ls.hideFollowers = function () {
    if (followers.length >= followers_to_show) {
        $("div[id^='user']").hide();
        if ((index + followers_to_show) > followers.length) {
            //this is for the last slice
            for (var i = (followers.length - followers_to_show); i < followers.length; i++)
                $("#user-" + i).show();
        }
        else {

            for (var i = index; i < (index + followers_to_show); i++) {
                $("#user-" + i).show();
            }

        }
    }
}

ls.showCard = function () {
    if (!(localStorage.getItem("tkn") === null)) {
        if (change_follower){
            if (index + 1 == followers.length){
                index = 0;
                if (followers_without_cards){
                    ls.slideShow();
                    return;
                }
            }
            else
                index = (index + 1) % followers.length;
        }
        ls.showUserCardAnimation(index);
        if ((index % followers_to_show) == 0) {
            ls.hideFollowers();
        }
        /*if (change_follower){
            if (index + 1 == followers.length)
                index = 0;
            else
                index = (index + 1) % followers.length;
        }*/
    }
    //if (followers_without_cards >= followers.length){
      //  ls.slideShow();
    //}
}

/*ls.showMapAnimation = function () {
    $("#card").toggle("clip", {direction:"horizontal"});
    setTimeout(function(){
        ls.showMap();
    }, 600);
}*/

ls.showMap = function () {
    if (!(localStorage.getItem("tkn") === null)) {
        //var options = { direction: 'left' };
        //$('#card').toggle('slide', options, 500);
        card.weatherImg = "img/" + ls.weatherImage(card.weather);
        var postcardLansdcape = true;
        var infoWidth = $(window).width();
        var infoTop = $(window).height();
        //console.log("W: " + infoWidth + " - T: " + infoTop);
        //console.log("WB: " + infoWidth*0.8 + " - TB: " + infoTop*0.7);
        card.infoDataHeight = infoTop*0.2;
        card.infoMapHeight = infoTop*0.8;

        card.infoBarWidth = infoWidth*0.8;
        card.infoBarTop = infoTop*0.8;
        card.infoCircleTop = infoTop*0.8-38;
        
        if (postcardLansdcape){
            card.infoStampTop = infoTop*0.04-5;
            card.infoStampLeft = infoWidth*0.85-5;
            card.infoUserPicTop = infoTop*0.04;
            card.infoUserPicLeft = infoWidth*0.85;
            card.infoDataTop = infoTop*0.05;
            card.infoDataLeft = infoWidth*0.25;
            card.infoWeatherTop = infoTop*0.45;
            card.infoWeatherLeft = infoWidth*0.4;

            card.infoAgeTop = infoTop*0.05;
            card.infoAgeLeft = infoWidth*0.02;            
        }else{
            card.infoStampTop = infoTop*0.125-10;
            card.infoStampLeft = infoWidth*0.75-10;
            card.infoUserPicTop = infoTop*0.125;
            card.infoUserPicLeft = infoWidth*0.75;
            card.infoDataTop = infoTop*0.5;
            card.infoDataLeft = infoWidth*0.7;
            card.infoWeatherTop = infoTop*0.25;
            card.infoWeatherLeft = infoWidth*0.25;

            card.infoAgeTop = infoTop*0.85;
            card.infoAgeLeft = infoWidth*0.7;            
        }
        card.cardAge = dateDiffInDays(card.created);

        var shortName = card.user.username.substring(0, card.user.username.indexOf(" "));
        var shortLocation = card.location;
        if (card.location.indexOf(",") > 0){
            if (card.location.indexOf(",") == card.location.lastIndexOf(",")){
                shortLocation = card.location.substring(card.location.indexOf(",") + 1);
            }else{
                shortLocation = card.location.substring(0, card.location.indexOf(",")) + card.location.substring(card.location.lastIndexOf(","));
            }
        }

        card.short_location = shortLocation;
        card.short_name = shortName;

//        console.log(card.created);
        //var diffDays = dateDiffInDays(card.created);
        //console.log("DAYS: " + diffDays);

        card.divWidth = infoWidth;
        card.divHeight = infoTop;

        //Info Bar vertical on the right
        //var result = new EJS({url: 'ejs/map.ejs'}).render(card);

        //Info Bar horizontal on the top
        var result = new EJS({url: 'ejs/mapH.ejs'}).render(card);
        $('#card').html(result);
        len_pics = 1;
        //$("#card").show();
        ls.resize();
        //$("#card").hide();
        /*setTimeout(function(){
            $("#card").toggle("clip", {direction:"horizontal"});
        }, 200);*/
        setTimeout(ls.showCard, (timer / 2));
    }
}

ls.updateCards = function (init) {
    //uopdates the cards, first time (init=true) loads followers and loads al their cards
    //if (init) {
        gapi.client.user.peopleifollow().execute(function (resp) {
            if (!resp.code) {
                var f_resp = resp.result;
                followers = f_resp.users;
                if (followers === undefined) {
                    ls.slideShow();
                    return;
                }
                localStorage.setItem("followers", JSON.stringify(followers));
                ls.showFollowers();
                ajax.PopulateListView("delete");

                //for all the followers update the current card.
                for (var i = 0; i < followers.length; i++) {
                    var follower = followers[i];
                    console.log("Follower: " + follower.id);
//                  Remove old cards
                    localStorage.removeItem("card-0-" + follower.id);
                    localStorage.removeItem("card-1-" + follower.id);
                    localStorage.removeItem("card-2-" + follower.id);

                    gapi.client.card.usercardsuserIdn({'userId': follower.id, 'n': 3}).execute(function (resp) {
//                      here we get the cards, we have to store it.
//                      don't use follower.id here. it's async (damn JS)
                        if (!resp.code) {
//                            stringify beacuse it works only with strings
                            for (var j = 0; j < resp.cards.length; j++) {
                                var cardVarName = "card-" + j + "-" + resp.cards[j].user.id;
                                localStorage.setItem(cardVarName, JSON.stringify(resp.cards[j]));
                            }
                            //localStorage.setItem("card-" + resp.user.id, JSON.stringify(resp));
                        }
                    });
                }
//                initialize currentcards
                var usercards = [];
//                for all the followers fake the card as -1
                $.each(followers, function (index, follower) {
//                    build the array
                    var card_j = {};

                    card_j.user = follower.id;
                    card_j.card = -1;
                    usercards.push(card_j);
                });
//                check if there's a new one
                gapi.client.card.checkcards({"usercards": usercards }).execute(function (resp) {
                    if (!resp.code) {
                        usercards = resp.usercards;
//                        store this info in the currentCards (it's an array of (userId,cardId,new)
                        localStorage.setItem("currentCards", JSON.stringify(usercards));
                    }
                });
                ////after 5 sec show the card, pretty sure there's something..
                if (init) {
                    setTimeout(ls.showCard, 5000);
                }
            }
        });
    //}
//  After version 2.4 we remove the false branch and reload all the cards from the serverß
    /*
    else {
//        second time, thus there are cards
        var currentCards = localStorage.getItem("currentCards");
        var usercards = JSON.parse(currentCards);
        //for all the currentcards, check if there's anything new,
        gapi.client.card.checkcards({"usercards": usercards }).execute(function (resp) {
            //gives the list of new, then store the new card.
            if (!resp.code) {
                usercards = resp.usercards;
                localStorage.setItem("currentCards", JSON.stringify(usercards));

                $.each(usercards, function (index, data) {
                    //here i've to substitute the id with the new one.
//                    TODO: highlitth if there's a new card
                    if (data.new == true)
                        gapi.client.card.usercarduserId({'userId': user}).execute(function (resp2) {
//                here we get the cards, we have to store it.
                            if (!resp2.code) {
                                localStorage.setItem("card-" + resp.user.id, JSON.stringify(resp2));
                            }
                        });
                });
            }
        });
    }
    */
    //    everyhour check new one.
    setTimeout(function () {
        //reset the variables that control card and follower cycles
        number_of_card = 0;
        change_follower = false;
        followers_without_cards = true;
        //reset the followers slideshow
        ls.updateCards(false);
    }, 1000 * 60 * 60);

}

ls.slideShow = function () {
    //We show a fixed slide show if the user does not have any followers yet
    if (!(localStorage.getItem("tkn") === null)) {  //Stop slide show if user logged out
        var cardIndex = Math.floor((Math.random() * 20) + 1);
        var cardPhoto = "img/slideShow/" + cardIndex + ".jpg";    
        card = JSON.parse('{}');
        card.photo = cardPhoto;
        var result = new EJS({url: 'ejs/slideShow.ejs'}).render(card);

        $('#card').html(result);
        ls.resize();
        setTimeout(ls.slideShow, (timer / 2));
    }
}

ls.showUserCardAnimation = function (id) {
    //$('#card').fadeOut();                 , {direction:"right"}
    $("#card").toggle("slide");
    setTimeout(function(){
        ls.showUserCard(id);
    }, 500);
}

ls.showUserCard = function (id) {    
    if (followers.length > 0){
        var id_u = followers[id].id;
        //get the card from storage
        var card_s = localStorage.getItem("card-" + number_of_card + "-" + id_u);
        if (card_s != undefined) {
            card = JSON.parse(card_s);
            len_pics = card.photo.length;
            var i = Math.floor(Math.random() * (len_pics - number_of_pic));
            card.photo = card.photo.slice(i, i + number_of_pic);
            len_pics = card.photo.length;

            var infoWidth = $(window).width();
            var infoTop = $(window).height();
            
            //Bar on bottom layout
            card.infoBarWidth = infoWidth + 5;
            card.infoBarTop = infoTop-100;
            card.infoCircleTop = infoTop*0.8-38;
            card.divWidth = infoWidth;
            card.divHeight = infoTop;
            card.cardAge = dateDiffInDays(card.created);
            
            //cosnole.log("CARD USER PIC: " + card.user.picture);
            card.weatherImg = "img/" + ls.weatherImage(card.weather);
            var result = new EJS({url: 'ejs/card.ejs'}).render(card);
            $('#card').html(result);
            var name_div = "#user-" + id;
            $("#followers .active").removeClass("active");
            $(name_div).addClass("active");
            //Animate changing postcards
            $("#card").show();
            ls.resize();
            $("#card").hide();
            setTimeout(function(){
                $("#card").toggle("slide");
            }, 200);

            number_of_card = number_of_card + 1;
            console.log("Card Number: " + number_of_card);
            change_follower = false;
            followers_without_cards = false;
            setTimeout(ls.showMap, (timer / 2));
        }else{
            console.log("No cards");
            number_of_card = 0;
            change_follower = true;
            //followers_without_cards = followers_without_cards + 1;
            setTimeout(ls.showCard, 2000);
        }        
    }else{
        console.log("No Followers");
    }
}

ls.weatherImage = function (weather) {
    var weatherImg;
    switch (weather) {
        case "Breezy":
            weatherImg = "breezy.png";
            break;
        case "Breezy and Dry":
            weatherImg = "breezy-dry.png";
            break;
        case "Breezy and Mostly Cloudy":
            weatherImg = "breezy-mostly-cloudy.png";
            break;
        case "Breezy and Partly Cloudy":
            weatherImg = "breezy-partly-cloudy.png";
            break;
        case "Clear":
            weatherImg = "Clear.png";
            break;
        case "Drizzle":
            weatherImg = "drizzle.png";
            break;
        case "Drizzle and Breezy":
            weatherImg = "drizzle-breezy.png";
            break;
        case "Drizzle and Windy":
            weatherImg = "drizzle-windy.png";
            break;
        case "Dry":
            weatherImg = "dry.png";
            break;
        case "Dry and Mostly Cloudy":
            weatherImg = "dry-mostly-cloudy.png";
            break;
        case "Dry and Overcast":
            weatherImg = "dry-overcast.png";
            break;
        case "Dry and Partly Cloudy":
            weatherImg = "dry-partly-cloudy.png";
            break;
        case "Flurries":
            weatherImg = "flurries.png";
            break;
        case "Foggy":
            weatherImg = "foggy.png";
            break;
        case "Heavy Rain":
            weatherImg = "heavy-rain.png";
            break;
        case "Humid":
            weatherImg = "humid.png";
            break;
        case "Humid and Mostly Cloudy":
            weatherImg = "humid-mostly-cloudy.png";
            break;
        case "Humid and Overcast":
            weatherImg = "humid-overcast.png";
            break;
        case "Humid and Partly Cloudy":
            weatherImg = "humid-partly-cloudy.png";
            break;
        case "Light Rain":
            weatherImg = "light-rain.png";
            break;
        case "Light Rain and Breezy":
            weatherImg = "light-rain-breezy.png";
            break;
        case "Light Rain and Windy":
            weatherImg = "light-rain-windy.png";
            break;
        case "Light Snow":
            weatherImg = "light-snow.png";
            break;
        case "Mostly Cloudy":
            weatherImg = "mostly-cloudy.png";
            break;
        case "Overcast":
            weatherImg = "overcast.png";
            break;
        case "Partly Cloudy":
            weatherImg = "partly-cloudy.png";
            break;
        case "Rain":
            weatherImg = "rain.png";
            break;
        case "Rain and Breezy":
            weatherImg = "rain-breezy.png";
            break;
        case "Snow":
            weatherImg = "snow.png";
            break;
        case "Windy and Mostly Cloudy":
            weatherImg = "windy-mostly-cloudy.png";
            break;
        case "Windy and Partly Cloudy":
            weatherImg = "windy-partly-cloudy.png";
            break;
        default:
            weatherImg = "breezy-dry.png";
    }
    return weatherImg;
}


// dateCardString is a date in string format
function dateDiffInDays(dateCardString) {
    var timeFromCard = "";
    var _MS_PER_DAY = 1000 * 60 * 60 * 24;

    var yearCard = dateCardString.substring(0,4);
    var monthCard = parseInt(dateCardString.substring(5,7))-1;
    var dayCard = dateCardString.substring(8,10);
    var hourCard = dateCardString.substring(11,13);
    var minuteCard = dateCardString.substring(14,16);
    var secondCard = dateCardString.substring(17,19);
    var milisCard = dateCardString.substring(20);

    var a = new Date(yearCard, monthCard, dayCard, hourCard, minuteCard, secondCard, milisCard); 
    var b = new Date();

    // Discard the time and time-zone information.
    var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
    var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

    var daysAgo = Math.floor((utc2 - utc1) / _MS_PER_DAY);

    if (daysAgo <= 31){
        if (daysAgo < 1){
            //Today
            timeFromCard = globalizationApp.getLanguageValue("today");
        } else {
            if (daysAgo == 1){
                //1 day
                timeFromCard = daysAgo + " " + globalizationApp.getLanguageValue("1day");
            } else {
                //more than a day
                timeFromCard = daysAgo + " " + globalizationApp.getLanguageValue("xday");
            }
        }
    }else{
        var monthsAgo = Math.floor(daysAgo / 30);
        if (monthsAgo <= 1){
            //1 month
            timeFromCard = monthsAgo + " " + globalizationApp.getLanguageValue("1month");
        } else {
            //more than a month
            timeFromCard = monthsAgo + " " + globalizationApp.getLanguageValue("xmonth");
        }
    }

    return timeFromCard;
}