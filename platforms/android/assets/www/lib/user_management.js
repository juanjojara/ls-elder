var userManagement = userManagement || {};

userManagement.getUserData = function(){
    console.log("GET 1");
    gapi.client.user.me().execute(function(resp) {
        if (!resp.code) {
            $('#lblSettingsFooter, #lblFollowersFooter, #lblMainFooter').html(resp.username);
            $('#picSettingsFooter, #picFollowersFooter, #picMainFooter').attr("src", resp.picture);
            $('#selectLocLevel').val(resp.location_level).change();
            $('#selectShareControl').val(resp.sharing_level).change();
            console.log("USER LOAD OK!!!");
            localStorage.setItem("user_photo", resp.picture);
            localStorage.setItem("location_setting", resp.location_level);
            localStorage.setItem("sharing_setting", resp.sharing_level);
            localStorage.setItem("user_name", resp.username);
            //console.log(" PRE Configuring GeoLocation");

            if (window.plugins.backgroundGeoLocation) {
                console.log("AA");
                app.configureBackgroundGeoLocation();
                console.log("BB");
            }
            /*
            //Initializaing Background Service for Geo Location
            if (window.plugins.backgroundGeoLocation) {
                console.log("Configuring GeoLocation");
                lsService.configureBackgroundGeoLocation();
                console.log("Finished Configuring GeoLocation");
            }
            */
        }else{
            $("#error").html(resp.message);
        }
    });
}

userManagement.setUserData = function(loc_level, share_level){
    console.log("Settings " + loc_level + " - " + share_level);
    gapi.client.user.set_privacy({"location_level": loc_level, "sharing_level": share_level}).execute(function(resp) {
        if (!resp.code) {
            console.log("Settings saved OK");
            localStorage.setItem("location_setting", loc_level);
            localStorage.setItem("sharing_setting", share_level);
            //$('#savedSettingsPopup').popup('open');
            $("#savedSettingsPopup p").html("Settings saved succesfully");
            setTimeout(function () {
                console.log("Set popup Close");
                $('#savedSettingsPopup').popup('close');
             }, 3000);
        }else{
            $("#savedSettingsPopup p").html("Error saving settings");
            setTimeout(function () {
                $('#savedSettingsPopup').popup('close');
             }, 2000);
            $("#error").html(resp.message);
        }
    });
}
